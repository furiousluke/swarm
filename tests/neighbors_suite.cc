#include <libhpc/debug/unit_test_main.hh>
#include <swarm/neighbors.hh>
#include <swarm/grid.hh>
#include <swarm/locate.hh>

using namespace hpc::test;

namespace {

   test_case<> ANON(
      "/swarm/neighbor_cell_iterator/uniform/2d/1_radius",
      "",
      []()
      {
         typedef swarm::neighbor_cell_iterator<2,swarm::grids::uniform<2>> nci_type;

         swarm::grids::uniform<2> grid(
	    std::array<swarm::pos_type,2>{ 0.0, 0.0 },
            std::array<swarm::pos_type,2>{ 1.0, 1.0 },
            0.25
            );

         // Top left corner.
         {
            uint32_t results[] = { 0, 1, 5, 6 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.1, 0 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Top right corner.
         {
            uint32_t results[] = { 3, 4, 8, 9 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.1, 4 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Bottom left corner.
         {
            uint32_t results[] = { 15, 16, 20, 21 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.1, 20 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Bottom right corner.
         {
            uint32_t results[] = { 18, 19, 23, 24 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.1, 24 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Central.
         {
            uint32_t results[] = { 6, 7, 8, 11, 12, 13, 16, 17, 18 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.1, 12 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }
      }
      );

   test_case<> ANON(
      "/swarm/neighbor_cell_iterator/uniform/2d/2_radius",
      "",
      []()
      {
         typedef swarm::neighbor_cell_iterator<2,swarm::grids::uniform<2>> nci_type;

         swarm::grids::uniform<2> grid(
            std::array<swarm::pos_type,2>{ 0.0, 0.0 },
            std::array<swarm::pos_type,2>{ 1.0, 1.0 },
            0.25
            );

         // Top left corner.
         {
            uint32_t results[] = { 0, 1, 2, 5, 6, 7, 10, 11, 12 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.2, 0 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Top right corner.
         {
            uint32_t results[] = { 2, 3, 4, 7, 8, 9, 12, 13, 14 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.2, 4 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Bottom left corner.
         {
            uint32_t results[] = { 10, 11, 12, 15, 16, 17, 20, 21, 22 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.2, 20 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Bottom right corner.
         {
            uint32_t results[] = { 12, 13, 14, 17, 18, 19, 22, 23, 24 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.2, 24 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }

         // Central.
         {
            uint32_t results[] = { 0,   1,  2,  3,  4,
                                   5,   6,  7,  8,  9,
                                   10, 11, 12, 13, 14,
                                   15, 16, 17, 18, 19,
                                   20, 21, 22, 23, 24 };
            unsigned ii = 0;
            for( nci_type it( &grid, 0.2, 12 ); it != nci_type(); ++it, ++ii )
               TEST( *it == results[ii] );
         }
      }
      );

   // test_case<> ANON(
   //    "/swarm/morton_neighbor_cell_iterator/2d",
   //    "",
   //    []()
   //    {
   //       // Top left corner.
   //       {
   //          uint32_t results[] = { 0, 1, 2, 3 };
   //          unsigned ii = 0;
   //          for( swarm::morton_neighbor_cell_iterator<2> it( 4, 0 ); it != swarm::morton_neighbor_cell_iterator<2>(); ++it, ++ii )
   //             TEST( *it == results[ii] );
   //       }

   //       // Top right corner.
   //       {
   //          uint32_t results[] = { 4, 5, 6, 7 };
   //          unsigned ii = 0;
   //          for( swarm::morton_neighbor_cell_iterator<2> it( 4, 5 ); it != swarm::morton_neighbor_cell_iterator<2>(); ++it, ++ii )
   //             TEST( *it == results[ii] );
   //       }

   //       // Bottom left corner.
   //       {
   //          uint32_t results[] = { 8, 9, 10, 11 };
   //          unsigned ii = 0;
   //          for( swarm::morton_neighbor_cell_iterator<2> it( 4, 10 ); it != swarm::morton_neighbor_cell_iterator<2>(); ++it, ++ii )
   //             TEST( *it == results[ii] );
   //       }

   //       // Bottom right corner.
   //       {
   //          uint32_t results[] = { 12, 13, 14, 15 };
   //          unsigned ii = 0;
   //          for( swarm::morton_neighbor_cell_iterator<2> it( 4, 15 ); it != swarm::morton_neighbor_cell_iterator<2>(); ++it, ++ii )
   //             TEST( *it == results[ii] );
   //       }

   //       // Central.
   //       {
   //          uint32_t results[] = { 1, 4, 5, 3, 6, 7, 9, 12, 13 };
   //          unsigned ii = 0;
   //          for( swarm::morton_neighbor_cell_iterator<2> it( 4, 6 ); it != swarm::morton_neighbor_cell_iterator<2>(); ++it, ++ii )
   //             TEST( *it == results[ii] );
   //       }
   //    }
   //    );

}
