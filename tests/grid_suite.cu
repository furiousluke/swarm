#include <libhpc/unit_test/main.hh>
#include <swarm/grid.hh>

TEST_CASE_CUDA( "/swarm/uniform/constructor/default" )
{
   swarm::grids::uniform<3> grid;
   TEST_EQ( grid.n_dims(), 2 );
   TEST_EQ( grid.n_dims(), 3 );
   TEST_EQ( grid.n_dims(), 2 );
   TEST_EQ( grid.min()[0], 1.0 );
   // for( int ii = 0; ii < grid.n_dims(); ++ii )
   // {
   //    TEST_EQ( grid.min()[ii], 0.0 );
   //    // TEST( grid.max()[ii] == 0.0 );
   //    // TEST( grid.delta()[ii] == 0.0 );
   //    // TEST( grid.sides()[ii] == 0 );
   //    // TEST( grid.n_cells() == 0 );
   // }
}

// TEST_CASE_CUDA( "/swarm/uniform/constructor/sides" )
// {
//    boost::array<swarm::real_type,3> min = { 1, 1, 1 };
//    boost::array<swarm::real_type,3> max = { 2, 2, 2 };
//    boost::array<uint16_t,3> sides = { 3, 3, 3 };
//    swarm::grids::uniform<3> grid( min, max, sides );
//    for( int ii = 0; ii < grid.n_dims(); ++ii )
//    {
//       TEST( grid.min()[ii] == 1.0 );
//       TEST( grid.max()[ii] == 2.0 );
//       DELTA( grid.delta()[ii], 0.3333, 1e-4 );
//       TEST( grid.sides()[ii] == 3 );
//       TEST( grid.n_cells() == 27 );
//    }
// }

// TEST_CASE_CUDA( "/swarm/uniform/constructor/delta" )
// {
//    boost::array<swarm::real_type,3> min = { 1, 1, 1 };
//    boost::array<swarm::real_type,3> max = { 2, 2, 2 };
//    swarm::grids::uniform<3> grid( min, max, 0.3 );
//    for( int ii = 0; ii < grid.n_dims(); ++ii )
//    {
//       TEST( grid.min()[ii] == 1.0 );
//       TEST( grid.max()[ii] == 2.0 );
//       DELTA( grid.delta()[ii], 0.25, 1e-5 );
//       TEST( grid.sides()[ii] == 4 );
//       TEST( grid.n_cells() == 64 );
//    }
// }

// TEST_CASE_CUDA( "/swarm/uniform/constructor/copy" )
// {
//    boost::array<swarm::real_type,3> min = { 1, 1, 1 };
//    boost::array<swarm::real_type,3> max = { 2, 2, 2 };
//    swarm::grids::uniform<3> src( min, max, 0.3 );
//    swarm::grids::uniform<3> grid( src );
//    for( int ii = 0; ii < grid.n_dims(); ++ii )
//    {
//       TEST( grid.min()[ii] == 1.0 );
//       TEST( grid.max()[ii] == 2.0 );
//       DELTA( grid.delta()[ii], 0.25, 1e-5 );
//       TEST( grid.sides()[ii] == 4 );
//       TEST( grid.n_cells() == 64 );
//    }
// }

// TEST_CASE_CUDA( "/swarm/uniform/project" )
// {
//    boost::array<swarm::real_type,3> min = { 1, 1, 1 };
//    boost::array<swarm::real_type,3> max = { 2, 2, 2 };
//    boost::array<uint16_t,3> sides = { 3, 3, 3 };
//    swarm::grids::uniform<3> grid( min, max, sides );
//    for( uint16_t ii = 0; ii < grid.sides()[2]; ++ii )
//    {
//       for( uint16_t jj = 0; jj < grid.sides()[1]; ++jj )
//       {
//          for( uint16_t kk = 0; kk < grid.sides()[0]; ++kk )
//          {
//             boost::array<uint16_t,3> crds = { ii, jj, kk };
//             TEST( grid.project( crds ) == grid.sides()[0]*(kk*grid.sides()[1] + jj) + ii );
//          }
//       }
//    }
// }

// TEST_CASE_CUDA( "/swarm/uniform/lift" )
// {
//    boost::array<swarm::real_type,3> min = { 1, 1, 1 };
//    boost::array<swarm::real_type,3> max = { 2, 2, 2 };
//    boost::array<uint16_t,3> sides = { 3, 3, 3 };
//    swarm::grids::uniform<3> grid( min, max, sides );
//    for( uint32_t ii = 0; ii < grid.n_cells(); ++ii )
//    {
//       boost::array<uint16_t,3> crds = grid.lift( ii );
//       TEST( crds[2] == ii/(grid.sides()[0]*grid.sides()[1]) );
//       TEST( crds[1] == (ii%(grid.sides()[0]*grid.sides()[1]))/grid.sides()[0] );
//       TEST( crds[0] == ii%grid.sides()[0] );
//    }
// }
