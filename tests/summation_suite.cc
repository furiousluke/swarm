#include <libhpc/debug/unit_test_main.hh>
#include <libhpc/containers/num.hh>
#include <swarm/swarm.hh>

using namespace hpc::test;

namespace {

   test_case<> ANON(
      "/swarm/summation/cutoff_radius",
      "Central density value should have been contributed to "
      "only by itself.",
      []()
      {
         // Setup constants and types.
         constexpr unsigned n_dims   = 2;
         constexpr unsigned pop_size = 10;
         constexpr swarm::h_type h = 1.0;
         typedef swarm::kernels::cubic<n_dims> kernel_type;
         constexpr swarm::pos_type r_cut = h*kernel_type::radius;
         typedef swarm::data::fixed<n_dims>                       data_type;
         typedef swarm::grids::uniform<n_dims>                    grid_type;
         typedef swarm::populations::ordered<grid_type,data_type> population_type;

         // Create the grid we will be using.
         grid_type grid(
	    std::array<swarm::pos_type,n_dims>{ 0.0, 0.0 },
            std::array<swarm::pos_type,n_dims>{ 10.0, 10.0 },
            1.0
            );

         // We're using a mapped population to begin with,
         // the most naive implementation.
         population_type pop( grid, pop_size );

         // Initialise a circular population around a central point.
         std::get<0>( pop[0].data ) = 5.0;
         std::get<1>( pop[0].data ) = 5.0;
         std::get<2>( pop[0].data ) = 0.0;
         for( unsigned ii = 1; ii < pop_size; ++ii )
         {
            double angle = (static_cast<double>( ii )/static_cast<double>( pop_size - 1 ))*2.0*M_PI;
            std::get<0>( pop[ii].data ) = 5.0 + 1.0001*cos( angle )*r_cut;
            std::get<1>( pop[ii].data ) = 5.0 + 1.0001*sin( angle )*r_cut;
            std::get<2>( pop[ii].data ) = 0.0;
         }

         // Locate new population.
         pop.locate();

         // Currently using constant smoothing length.
         pop.set_smoothing_length( h );

         // Density summation.
         typedef swarm::interactions::density<data_type> inter_type;
         swarm::summation<population_type>()( pop, inter_type(), kernel_type() );

         // Locate the particle in the middle.
         unsigned mid_idx;
         for( unsigned ii = 0; ii < pop.size(); ++ii )
         {
            if( hpc::num::approx<swarm::pos_type>( std::get<0>( pop[ii].data ), 5.0, 1e-4 ) &&
                hpc::num::approx<swarm::pos_type>( std::get<1>( pop[ii].data ), 5.0, 1e-4 ) )
            {
               mid_idx = ii;
               break;
            }
         }
	 swarm::density_type res = 10.0/(7.0*M_PI);
         DELTA( std::get<2>( pop[mid_idx].data ), res, 1e-4 );
      }
      );

}
