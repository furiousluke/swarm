#include <libhpc/debug/unit_test_main.hh>
#include <libhpc/containers/num.hh>
#include <swarm/swarm.hh>

using namespace hpc::test;

namespace {

   test_case<> ANON(
      "/swarm/newton",
      "",
      []()
      {
	 LOG_PUSH( new hpc::logging::stdout( 0, "newton" ) );

         // Setup constants and types.
         constexpr unsigned n_dims   = 2;
         constexpr unsigned pop_size = 10;
         constexpr swarm::h_type h = 1.0;
         typedef swarm::kernels::cubic<n_dims> kernel_type;
         constexpr swarm::pos_type r_cut = h*kernel_type::radius;
         typedef swarm::data::adaptive<n_dims>                    data_type;
         typedef swarm::grids::uniform<n_dims>                    grid_type;
         typedef swarm::populations::ordered<grid_type,data_type> population_type;

         // Create the grid we will be using.
         grid_type grid(
	    std::array<swarm::pos_type,n_dims>{ 0.0, 0.0 },
            std::array<swarm::pos_type,n_dims>{ 10.0, 10.0 },
            1.0
            );

         // We're using a mapped population to begin with,
         // the most naive implementation.
         population_type pop( grid, pop_size );

         // Initialise a circular population around a central point.
         std::get<0>( pop[0].data ) = 5.0;
         std::get<1>( pop[0].data ) = 5.0;
         std::get<2>( pop[0].data ) = 0.0;
         for( unsigned ii = 1; ii < pop_size; ++ii )
         {
            double angle = (static_cast<double>( ii )/static_cast<double>( pop_size - 1 ))*2.0*M_PI;
            std::get<0>( pop[ii].data ) = 5.0 + 1.0001*cos( angle )*r_cut;
            std::get<1>( pop[ii].data ) = 5.0 + 1.0001*sin( angle )*r_cut;
            std::get<2>( pop[ii].data ) = 0.0;
            std::get<3>( pop[ii].data ) = h;
         }

         // Locate new population.
         pop.locate();

	 // Find the first cell with particles.
	 unsigned cell = 0;
	 while( pop.cell_size( cell ) == 0 )
	    ++cell;

         // // Create the newton function.
         // typedef swarm::interactions::density<data_type> inter_type;
	 // swarm::summation<population_type> sum( pop );
	 // inter_type inter;
	 // swarm::newton_function< swarm::summation<population_type>,
	 //                         swarm::interactions::density<data_type>,
	 //                         kernel_type > func(
	 //    sum,
	 //    inter,
	 //    kernel_type(),
	 //    cell,
	 //    &pop.cell_particle( cell, 0 )
	 //    );

	 // std::cout << "\n";
	 // for( unsigned ii = 0; ii < 1000; ++ii )
	 // {
	 //    double x = static_cast<double>( ii )/99.9;
	 //    double f = func( x );
	 //    std::cout << x << " " << f << " " << func.derivative( x, f ) << "\n";
	 // }

         // // Run through the newton solver.
         // double new_h = hpc::num::newton( func, 0.0, 100.0 );
      }
      );

}
