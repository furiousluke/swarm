#include <libhpc/unit_test/main.hh>
#include <libhpc/system/random.hh>
#include <swarm/interaction.hh>
#include <swarm/data.hh>

TEST_CASE( "/swarm/interaction/density" )
{
   typedef swarm::data::adaptive<2> data_type;
   swarm::interactions::density<data_type> inter;
   data_type part_i, part_j;
   std::get<0>( part_i.data ) = 0.0;
   std::get<1>( part_i.data ) = 0.0;
   std::get<2>( part_i.data ) = 0.0;
   std::get<3>( part_i.data ) = 0.0;
   std::get<0>( part_j.data ) = 0.0;
   std::get<1>( part_j.data ) = 0.0;
   std::get<2>( part_j.data ) = 0.0;
   std::get<3>( part_j.data ) = 0.0;

   inter( part_i, part_j, 2.0, 3.0 );
   TEST( std::get<0>( part_i.data ) == 0.0 );
   TEST( std::get<1>( part_i.data ) == 0.0 );
   DELTA( std::get<2>( part_i.data ), 6.0, 1e-8 );
   TEST( std::get<3>( part_i.data ) == 0.0 );
   TEST( std::get<0>( part_j.data ) == 0.0 );
   TEST( std::get<1>( part_j.data ) == 0.0 );
   TEST( std::get<2>( part_j.data ) == 0.0 );
   TEST( std::get<3>( part_j.data ) == 0.0 );

   inter( part_j, part_i, 3.0, 2.0 );
   TEST( std::get<0>( part_i.data ) == 0.0 );
   TEST( std::get<1>( part_i.data ) == 0.0 );
   DELTA( std::get<2>( part_i.data ), 6.0, 1e-8 );
   TEST( std::get<3>( part_i.data ) == 0.0 );
   TEST( std::get<0>( part_j.data ) == 0.0 );
   TEST( std::get<1>( part_j.data ) == 0.0 );
   DELTA( std::get<2>( part_j.data ), 6.0, 1e-8 );
   TEST( std::get<3>( part_j.data ) == 0.0 );
}
