#include <libhpc/unit_test/main.hh>
#include <libhpc/system/random.hh>
#include <swarm/kernel.hh>

TEST_CASE( "/swarm/kernel/cubic/2d" )
{
   swarm::real_type norm = 10/(7.0*M_PI);
   swarm::kernels::cubic<2> kern;
   for( swarm::real_type h = 1.0; h < 5.0; h += 1.0 )
   {
      // Do some simple value checks.
      DELTA( kern( (0.5*h)*(0.5*h), h ), norm*(0.84375 - 0.125)/(h*h), 1e-5, "First band." );
      DELTA( kern( (1.5*h)*(1.5*h), h ), norm*0.03125/(h*h), 1e-5, "Second band." );
      DELTA( kern( (2.5*h)*(2.5*h), h ), 0.0, 1e-5, "Outside radius." );
   }
}

TEST_CASE( "/swarm/kernel/cubic/2d/h_derivative" )
{
   swarm::real_type norm = 10/(7.0*M_PI);
   swarm::kernels::cubic<2> kern;
   for( swarm::real_type h = 1.0; h < 5.0; h += 1.0 )
   {
      swarm::real_type eps = 1e-6;
      swarm::real_type hl = h - eps;
      swarm::real_type hu = h + eps;

      auto res = kern.deriv_h( (0.5*h)*(0.5*h) , h );
      DELTA( res[0], norm*(0.84375 - 0.125)/(h*h), 1e-5, "First band." );
      swarm::real_type deriv = (kern( (0.5*h)*(0.5*h), hu ) - kern( (0.5*h)*(0.5*h), hl ))/(2.0*eps);
      DELTA( deriv, res[1], 1e-5, "Derivative." );

      res = kern.deriv_h( (1.5*h)*(1.5*h), h );
      DELTA( res[0], norm*0.03125/(h*h), 1e-5, "Second band." );
      deriv = (kern( (1.5*h)*(1.5*h), hu ) - kern( (1.5*h)*(1.5*h), hl ))/(2.0*eps);
      DELTA( deriv, res[1], 1e-5, "Derivative." );

      res = kern.deriv_h( (2.5*h)*(2.5*h), h );
      DELTA( res[0], 0.0, 1e-5, "Outside radius." );
      deriv = (kern( (2.5*h)*(2.5*h), hu ) - kern( (2.5*h)*(2.5*h), hl ))/(2.0*eps);
      DELTA( deriv, res[1], 1e-5, "Derivative." );
   }
}

TEST_CASE( "/swarm/kernel/cubic/3d" )
{
   swarm::real_type norm = 1/M_PI;
   swarm::kernels::cubic<3> kern;
   for( swarm::real_type h = 1.0; h < 5.0; h += 1.0 )
   {
      // Do some simple value checks.
      DELTA( kern( (0.5*h)*(0.5*h), h ), norm*(0.84375 - 0.125)/(h*h*h), 1e-5, "First band." );
      DELTA( kern( (1.5*h)*(1.5*h), h ), norm*0.03125/(h*h*h), 1e-5, "Second band." );
      DELTA( kern( (2.5*h)*(2.5*h), h ), 0.0, 1e-5, "Outside radius." );
   }
}

TEST_CASE( "/swarm/kernel/cubic/3d/h_derivative" )
{
   swarm::real_type norm = 1.0/M_PI;
   swarm::kernels::cubic<3> kern;
   for( swarm::real_type h = 1.0; h < 5.0; h += 1.0 )
   {
      swarm::real_type hl = h - 1e-6;
      swarm::real_type hu = h + 1e-6;

      auto res = kern.deriv_h( (0.5*h)*(0.5*h) , h );
      DELTA( res[0], norm*(0.84375 - 0.125)/(h*h*h), 1e-5, "First band." );
      swarm::real_type deriv = (kern( (0.5*h)*(0.5*h), hu ) - kern( (0.5*h)*(0.5*h), hl ))/2e-6;
      DELTA( deriv, res[1], 1e-5, "Derivative." );

      res = kern.deriv_h( (1.5*h)*(1.5*h), h );
      DELTA( res[0], norm*0.03125/(h*h*h), 1e-5, "Second band." );
      deriv = (kern( (1.5*h)*(1.5*h), hu ) - kern( (1.5*h)*(1.5*h), hl ))/2e-6;
      DELTA( deriv, res[1], 1e-5, "Derivative." );

      res = kern.deriv_h( (2.5*h)*(2.5*h), h );
      DELTA( res[0], 0.0, 1e-5, "Outside radius." );
      deriv = (kern( (2.5*h)*(2.5*h), hu ) - kern( (2.5*h)*(2.5*h), hl ))/2e-6;
      DELTA( deriv, res[1], 1e-5, "Derivative." );
   }
}
