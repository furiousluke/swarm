#include <libhpc/debug/unit_test_main.hh>
#include <swarm/locate.hh>
#include <swarm/grid.hh>

using namespace hpc::test;

struct dummy
{
   std::tuple<double,double> data;
};

namespace {

   test_case<> ANON(
      "/swarm/locate/2d/uniform/small",
      "",
      []()
      {
         swarm::grids::uniform<2> grid( { 0.0, 0.0 }, { 4.0, 4.0 }, 1.2 );
         std::vector<dummy> data( 16 );
         for( unsigned ii = 0; ii < 4; ++ii )
         {
            for( unsigned jj = 0; jj < 4; ++jj )
            {
               unsigned idx = ii*4 + jj;
               std::get<0>( data[idx].data ) = 0.5 + (double)jj;
               std::get<1>( data[idx].data ) = 0.5 + (double)ii;
            }
         }
         std::vector<unsigned> bins( 16 );
         swarm::locate<2>( data.begin(), data.end(), grid, bins );
         unsigned results[] = { 0,   1,  2,  3,
                                4,   5,  6,  7,
                                8,   9, 10, 11,
                                12, 13, 14, 15 };
         for( unsigned ii = 0; ii < 16; ++ii )
            TEST( bins[ii] == results[ii] );
      }
      );

   test_case<> ANON(
      "/swarm/locate/2d/morton/small",
      "",
      []()
      {
         swarm::grids::morton<2> grid( { 0.0, 0.0 }, { 4.0, 4.0 }, 1.2 );
         std::vector<dummy> data( 16 );
         for( unsigned ii = 0; ii < 4; ++ii )
         {
            for( unsigned jj = 0; jj < 4; ++jj )
            {
               unsigned idx = ii*4 + jj;
               std::get<0>( data[idx].data ) = 0.5 + (double)jj;
               std::get<1>( data[idx].data ) = 0.5 + (double)ii;
            }
         }
         std::vector<unsigned> bins( 16 );
         swarm::locate<2>( data.begin(), data.end(), grid, bins );
         unsigned results[] = { 0,   1,  4,  5,
                                2,   3,  6,  7,
                                8,   9, 12, 13,
                                10, 11, 14, 15 };
         for( unsigned ii = 0; ii < 16; ++ii )
            TEST( bins[ii] == results[ii] );
      }
      );

   test_case<> ANON(
      "/swarm/locate/2d/morton/large",
      "",
      []()
      {
         swarm::grids::morton<2> grid( { 0.0, 0.0 }, { 8.0, 8.0 }, 1.04 );
         std::vector<dummy> data( 64 );
         for( unsigned ii = 0; ii < 8; ++ii )
         {
            for( unsigned jj = 0; jj < 8; ++jj )
            {
               unsigned idx = ii*8 + jj;
               std::get<0>( data[idx].data ) = 0.5 + (double)jj;
               std::get<1>( data[idx].data ) = 0.5 + (double)ii;
            }
         }
         std::vector<unsigned> bins( 64 );
         swarm::locate<2>( data.begin(), data.end(), grid, bins );
         unsigned results[] = { 0,   1,  4, 5,  16, 17, 20, 21,
                                2,   3,  6, 7,  18, 19, 22, 23,
                                8,   9, 12, 13, 24, 25, 28, 29,
                                10, 11, 14, 15, 26, 27, 30, 31,
                                32, 33, 36, 37, 48, 49, 52, 53,
                                34, 35, 38, 39, 50, 51, 54, 55,
                                40, 41, 44, 45, 56, 57, 60, 61,
                                42, 43, 46, 47, 58, 59, 62, 63 };
         for( unsigned ii = 0; ii < 64; ++ii )
            TEST( bins[ii] == results[ii] );
      }
      );

   test_case<> ANON(
      "/swarm/locate/calc_cell_displs",
      "",
      []()
      {
         std::vector<unsigned> bins( 10 );
         bins[0] = 1;
         bins[1] = 1;
         bins[2] = 2;
         bins[3] = 2;
         bins[4] = 2;
         bins[5] = 4;
         bins[6] = 4;
         bins[7] = 4;
         bins[8] = 4;
         bins[9] = 4;
         std::vector<unsigned> displs( 7 );
         swarm::calc_cell_displs( bins, displs );
         unsigned results[] = { 0, 0, 2, 5, 5, 10, 10 };
         for( unsigned ii = 0; ii < displs.size(); ++ii )
            TEST( displs[ii] == results[ii] );
      }
      );

}
