#include <libhpc/mpi/unit_test_main.hh>
#include <swarm/scatter.hh>
#include <swarm/grid.hh>
#include <swarm/data.hh>
#include <swarm/population.hh>
#include <swarm/populate.hh>
#include <swarm/mpi/grid.hh>
#include <swarm/xdmf.hh>

using namespace hpc::test;

struct dummy
{
   std::tuple<double,double> data;
};

namespace {

   test_case<> ANON(
      "/swarm/scatter/2d/uniform/small",
      "",
      []()
      {
   	 typedef swarm::data::adaptive<2>                           data_type;
   	 typedef swarm::mpi::grids::block<swarm::grids::uniform<2>> grid_type;
   	 typedef swarm::populations::ordered<grid_type,data_type>   population_type;
   	 grid_type grid( std::array<swarm::pos_type,2>( { 0.0, 0.0 } ),
   			 std::array<swarm::pos_type,2>( { 4.0, 4.0 } ),
   			 1.2 );
   	 population_type pop( grid, 3 );
   	 swarm::populate::grid_cell( pop );
   	 pop.locate();
   	 pop.scatter<grid_type>();
   	 for( unsigned ii = 0; ii < grid.n_cells(); ++ii )
   	 {
   	    auto crd = grid.lift( ii );
   	    std::array<double,2> pos;
   	    pos[0] = grid.min()[0] + ((double)crd[0] + 0.5)*grid.delta()[0];
   	    pos[1] = grid.min()[1] + ((double)crd[1] + 0.5)*grid.delta()[1];
   	    DELTA( std::get<0>( pop[ii].data ), pos[0], 1e-5 );
   	    DELTA( std::get<1>( pop[ii].data ), pos[1], 1e-5 );
   	 }
      }
      );

   test_case<> ANON(
      "/swarm/scatter/2d/uniform/small/cells",
      "",
      []()
      {
   	 typedef swarm::data::adaptive<2>                           data_type;
   	 typedef swarm::mpi::grids::block<swarm::grids::uniform<2>> grid_type;
   	 typedef swarm::populations::ordered<grid_type,data_type>   population_type;
   	 grid_type grid( std::array<swarm::pos_type,2>( { 0.0, 0.0 } ),
   			 std::array<swarm::pos_type,2>( { 4.0, 4.0 } ),
   			 1.2 );
   	 population_type pop( grid, 3, true );
   	 swarm::populate::grid_cell( pop );
   	 pop.locate();
	 LOGILN( "PRECELLS: ", pop.cells() );
   	 pop.scatter<grid_type>();
	 LOGILN( "POSCELLS: ", pop.cells() );
   	 for( unsigned ii = 0; ii < grid.n_cells(); ++ii )
   	 {
   	    auto crd = grid.lift( ii );
   	    std::array<double,2> pos;
   	    pos[0] = grid.min()[0] + ((double)crd[0] + 0.5)*grid.delta()[0];
   	    pos[1] = grid.min()[1] + ((double)crd[1] + 0.5)*grid.delta()[1];
   	    DELTA( std::get<0>( pop[ii].data ), pos[0], 1e-5 );
   	    DELTA( std::get<1>( pop[ii].data ), pos[1], 1e-5 );
	    TEST( pop.cells()[ii] == ii );
   	 }
      }
      );

   test_case<> ANON(
      "/swarm/scatter/2d/uniform/large",
      "",
      []()
      {
   	 typedef swarm::data::adaptive<2>                           data_type;
   	 typedef swarm::mpi::grids::block<swarm::grids::uniform<2>> grid_type;
   	 typedef swarm::populations::ordered<grid_type,data_type>   population_type;
   	 grid_type grid( std::array<swarm::pos_type,2>( { 0.0, 0.0 } ),
   			 std::array<swarm::pos_type,2>( { 10.0, 10.0 } ),
   			 1.01 );
   	 population_type pop( grid, 3 );
   	 swarm::populate::grid_cell( pop );
   	 pop.locate();
   	 pop.scatter<grid_type>();
   	 for( unsigned ii = 0; ii < grid.n_cells(); ++ii )
   	 {
   	    auto crd = grid.lift( ii );
   	    std::array<double,2> pos;
   	    pos[0] = grid.min()[0] + ((double)crd[0] + 0.5)*grid.delta()[0];
   	    pos[1] = grid.min()[1] + ((double)crd[1] + 0.5)*grid.delta()[1];
   	    DELTA( std::get<0>( pop[ii].data ), pos[0], 1e-5 );
   	    DELTA( std::get<1>( pop[ii].data ), pos[1], 1e-5 );
   	 }
      }
      );

}
