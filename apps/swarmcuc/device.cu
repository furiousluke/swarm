#include <boost/type_traits/integral_constant.hpp>
#include <thrust/device_vector.h>
#include <thrust/tuple.h>
#include <thrust/execution_policy.h>
#include <libhpc/algorithm/uniform.hh>
#include <swarm/kernel.hh>
#include <swarm/interaction.hh>
#include <swarm/types.hh>

#define BLOCK_SIZE 1000
#define TILE_SIZE  512

struct data_type
{
   enum { n_dims = 3 };
   struct constant_h : boost::false_type {};
   struct massles    : boost::true_type  {};
   thrust::tuple<swarm::pos_type,swarm::pos_type,swarm::pos_type,swarm::density_type,swarm::h_type> data;
};

template< class Interaction,
	  class Kernel >
struct information
{
   typedef Kernel kernel_type;

   information( data_type* data,
		uint32_t* displs,
		uint32_t n_cells,
		uint16_t grid_i,
		uint16_t grid_j,
		uint16_t grid_k,
		swarm::pos_type delta_i,
		swarm::pos_type delta_j,
		swarm::pos_type delta_k,
		swarm::h_type eta = 1.0 )
      : data( data ),
	displs( displs ),
	n_cells( n_cells ),
	eta( eta )
   {
      grid_sides = hpc::make_array( grid_i, grid_j, grid_k );
      grid_delta = hpc::make_array( delta_i, delta_j, delta_k );
   }

   data_type* data;
   uint32_t* displs;
   uint32_t n_cells;
   hpc::array<ushort,3> grid_sides;
   hpc::array<swarm::pos_type,3> grid_delta;
   swarm::h_type deriv_h;
   swarm::h_type eta;
   Interaction inter;
   Kernel kern;
};

typedef information< swarm::interactions::density<data_type>, swarm::kernels::cubic<3> > info_type;

__device__ __host__ __forceinline__
void
eval_kernel( info_type& info,
	     data_type& d_i,
	     data_type& d_j,
	     swarm::pos_type r_ij_sq,
	     swarm::h_type h_i )
{
   hpc::array<swarm::h_type,2> val = info.kern.deriv_h( r_ij_sq, h_i );
   info.inter( d_i, d_j, 1.0, val[0] );
   info.deriv_h += val[1];
}

__device__ __host__ __forceinline__
void
interaction( info_type& info,
	     data_type& d_i,
	     data_type& d_j,
	     swarm::h_type h_i,
	     swarm::pos_type r_cut )
{
   swarm::pos3_type x_i = make_pos3( thrust::get<0>( d_i.data ),
				     thrust::get<1>( d_i.data ),
				     thrust::get<2>( d_i.data ) );
   swarm::pos3_type x_j = make_pos3( thrust::get<0>( d_j.data ),
				     thrust::get<1>( d_j.data ),
				     thrust::get<2>( d_j.data ) );
   swarm::pos3_type x_ij = make_pos3( x_j.x - x_i.x, x_j.y - x_i.y, x_j.z - x_i.z );
   swarm::pos_type r_ij_sq = x_ij.x*x_ij.x + x_ij.y*x_ij.y + x_ij.z*x_ij.z;
   swarm::pos_type r_cut_sq = r_cut*r_cut;
   if( r_ij_sq < r_cut_sq )
      eval_kernel( info, d_i, d_j, r_ij_sq, h_i );
}

__device__ __forceinline__
void
process_neighbor_cell( info_type& info,
		       data_type& d_i,
		       uint nbr,
		       swarm::h_type h_i,
		       swarm::pos_type r_cut )
{
   __shared__ data_type nbr_data[TILE_SIZE];

   // Get the particle neighbor indices.
   uint part_begin = info.displs[nbr];
   uint part_end   = info.displs[nbr + 1];

   // Now the magic. We need stage the particles into shared memory
   // as coalesced as possible.
   for( uint my_part = part_begin + threadIdx.x; my_part < part_end; my_part += blockDim.x )
   {
      if( my_part >= part_end )
	 break;

      // Transfer to shared.
      nbr_data[threadIdx.x] = info.data[my_part];

      // Need to sync here to prevent race conditions.
      __syncthreads();

      // How big is the current tile?
      uint cur_tile_size = ((part_end - part_begin) < blockDim.x) ? (part_end - part_begin) : blockDim.x;
      part_begin += cur_tile_size;

      // Iterate over the current tile.
      for( unsigned p_j = 0; p_j < cur_tile_size; ++p_j )
	 interaction( info, d_i, nbr_data[p_j], h_i, r_cut );
   }
}

__global__
void
sum_kernel( info_type info )
{
   // Begin loop to cover all cells.
   for( uint32_t cell = blockIdx.x; cell < info.n_cells; cell += blockDim.x )
   {
      if( cell >= info.n_cells )
   	 break;

      // Get the grid origin for this cell.
      hpc::array<ushort,3> org = hpc::lift<uint32_t,ushort,3,ushort>( cell, info.grid_sides );

      // Calculate the particle begin and end positions.
      uint32_t part_begin = info.displs[cell];
      uint32_t part_end   = info.displs[cell + 1];

      // Track neighbor cell coordinate.
      hpc::array<ushort,3> nbr_crd;

      // Begin loop to cover all particles.
      for( unsigned p_i = part_begin + threadIdx.x; p_i < part_end; p_i += blockDim.x )
      {
   	 if( p_i >= part_end )
   	    break;

   	 // Cache some things for performance.
   	 data_type d_i = info.data[p_i];
   	 swarm::h_type h = thrust::get<4>( d_i.data );

   	 // Run a Newton-Raphson solve.
   	 swarm::h_type delta;
   	 uint its = 0;
   	 do
   	 {
   	    // Clear particle data before summation.
   	    thrust::get<3>( d_i.data ) = 0.0;
   	    info.deriv_h = 0.0;

   	    // Calculate grid metrics.
   	    swarm::pos_type r_cut = h*(swarm::pos_type)info_type::kernel_type::radius;
   	    ushort3 step = make_ushort3( (ushort)(r_cut/info.grid_delta[0]) + 1,
   					 (ushort)(r_cut/info.grid_delta[1]) + 1,
   					 (ushort)(r_cut/info.grid_delta[2]) + 1 );
   	    ushort3 low = make_ushort3( (org[0] > step.x) ? (org[0] - step.x) : 0,
   					(org[1] > step.y) ? (org[1] - step.y) : 0,
   					(org[2] > step.z) ? (org[2] - step.z) : 0 );
   	    ushort3 upp = make_ushort3( (info.grid_sides[0] - org[0] > step.x) ? (org[0] + step.x + 1) : info.grid_sides[0],
   					(info.grid_sides[1] - org[1] > step.y) ? (org[1] + step.y + 1) : info.grid_sides[1],
   					(info.grid_sides[2] - org[2] > step.z) ? (org[2] + step.z + 1) : info.grid_sides[2] );

   	    // Iterate over neighboring grid cells.
   	    for( nbr_crd[2] = low.z; nbr_crd[2] < upp.z; ++nbr_crd[2] )
   	    {
   	       for( nbr_crd[1] = low.y; nbr_crd[1] < upp.y; ++nbr_crd[1] )
   	       {
   		  for( nbr_crd[0] = low.x; nbr_crd[0] < upp.x; ++nbr_crd[0] )
   		  {
   		     // Project to the flattened grid index and process.
   		     uint idx = hpc::project<ushort,uint,3,ushort>( nbr_crd, info.grid_sides );
   		     process_neighbor_cell( info, d_i, idx, h, r_cut );
   		  }
   	       }
   	    }

   	    // Claculate the Newton-Raphson values and check if we're done.
   	    swarm::density_type rho = thrust::get<3>( d_i.data );
   	    swarm::h_type new_h = info.eta*swarm::pow<swarm::density_type>( 1.0/rho, 1.0/3.0 );
   	    swarm::h_type f = h - new_h;
   	    swarm::h_type dfdh = (info.eta*info.deriv_h*swarm::pow<swarm::density_type>( 1.0/rho, 1.0/3.0 - 1.0 ))/(3.0*rho*rho) + 1.0;
   	    delta = f/dfdh;
   	    h -= delta;
   	 }
   	 while( fabs( delta ) > 1e-5 && ++its < 60 );

   	 // Update h and other data values.
   	 thrust::get<3>( info.data[p_i].data ) = thrust::get<3>( d_i.data );
   	 thrust::get<4>( info.data[p_i].data ) = h;
      }
   }
}

void
cuda_summation( thrust::host_vector<data_type>& host_data,
		thrust::host_vector<uint32_t> const& host_displs,
		uint16_t grid_i,
		uint16_t grid_j,
		uint16_t grid_k,
		swarm::pos_type delta_i,
		swarm::pos_type delta_j,
		swarm::pos_type delta_k )
{
   thrust::device_vector<data_type> data( host_data );
   thrust::device_vector<uint32_t> displs( host_displs );

   sum_kernel<<< BLOCK_SIZE, TILE_SIZE >>>(
      info_type(
	 thrust::raw_pointer_cast( data.data() ),
	 thrust::raw_pointer_cast( displs.data() ),
	 displs.size() - 1,
	 grid_i, grid_j, grid_k,
	 delta_i, delta_j, delta_k
	 )
      );
   if( cudaPeekAtLastError() != cudaSuccess )
   {
      std::cout << "Launch failure.\n";
      abort();
   }
   if( cudaDeviceSynchronize() != cudaSuccess )
   {
      std::cout << "Kernel failure.\n";
      abort();
   }

   host_data = data;
}
