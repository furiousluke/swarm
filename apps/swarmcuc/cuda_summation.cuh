#ifndef swarm_cuda_summation_cuh
#define swarm_cuda_summation_cuh

__global__
void
summation_3d()
{
}

namespace swarm {
   namespace cuda {

      // __global__
      // template< class Data,
      // 		class Interaction,
      // 		class Kernel >
      // void
      // summation_3d( uint32_t n_cells,
      // 		    uint16_t grid_sides[3],
      // 		    float grid_delta[3],
      // 		    Data[] data,
      // 		    uint32_t[] part_displs,
      // 		    Interaction const& inter,
      // 		    Kernel const& kern )
      // {
      // 	 // Calculate the cell range I will compute.
      // 	 uint32_t cell_begin = blockIdx.x*n_cells/blockDim.x;
      // 	 uint32_t cell_end   = (blockIdx.x + 1)*n_cells/blockDim.x;

      // 	 // Iterate over all grid cells.
      // 	 for( uint32_t ii = cell_begin; ii < cell_end; ++ii )
      // 	 {
      // 	    // Cache cell information.
      // 	    uint32_t n_parts_i = n_parts[ii];

      // 	    // Don't process a cell if it is empty, or if my thread index
      // 	    // indicates I am not involved.
      // 	    if( !n_parts_i || threadIdx.x >= n_parts_i )
      // 	       continue;

      // 	    // Calculate the assignment of particles to threads.
      // 	    uint32_t part_begin = threadIdx.x*n_parts_i/threadDim.x;
      // 	    uint32_t part_end   = (threadIdx.x + 1)*n_parts_i/threadDim.x;

      // 	    // Process my range of particles.
      // 	    for( uint32_t jj = part_begin; jj < part_end; ++jj )
      // 	    {
      // 	       data_type& data_i = data[jj];

      // 	       // Prepare initial values.
      // 	       float32 h = thrust::get<4>( data_i );
      // 	       float32 rho = 0.0;

      // 	       // Update values.
      // 	       thrust::get<4>( data_i ) = h;
      // 	       thrust::get<3>( data_i ) = rho;
      // 	    }
      // 	 }
      // }

      // __device__
      // void
      // process_neighbor_cell()
      // {
      // 	 // Load all particles in neighbor cell into shared memory.
      // 	 // __sync_threads();

      // 	 for( unsigned ii = 0; ii < n_nbr_parts; ++ii )
      // 	    interaction( inter, kern, data_i, *n_it, h, r_cut );
      // }

      __device__
      void
      process_particle( int low_i, int upp_i,
			int low_j, int upp_j,
			int low_k, int upp_k )
      {

      }

   }
}

#endif
