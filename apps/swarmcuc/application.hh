#include <libhpc/mpi/application.hh>

class application
   : public hpc::mpi::application
{
public:

   application( int argc,
                char* argv[] );

   void
   operator()();
};
