#include <boost/type_traits/integral_constant.hpp>
#include <thrust/host_vector.h>
#include <thrust/tuple.h>
#include <libhpc/containers/mymath.hh>
#include <libhpc/system/stream_output.hh>
#include <libhpc/profile/timer.hh>
#include <libhpc/mpi/logger.hh>
#include <swarm/swarm.hh>
#include "application.hh"

constexpr unsigned n_dims   = 3;
constexpr unsigned pop_size = 100000;
constexpr swarm::h_type   h        = 0.3;
constexpr swarm::h_type   eta      = 1.0;

struct data_type
{
   enum { n_dims = 3 };
   struct constant_h : boost::false_type {};
   struct massles    : boost::true_type  {};
   thrust::tuple<swarm::pos_type,swarm::pos_type,swarm::pos_type,swarm::density_type,swarm::h_type> data;
};

typedef swarm::kernels::cubic<n_dims>                           kernel_type;
typedef swarm::mpi::grids::block<swarm::grids::uniform<n_dims>> grid_type;
typedef swarm::populations::ordered<grid_type,data_type,thrust::host_vector> population_type;
typedef swarm::interactions::density<data_type>                 interaction_type;

constexpr swarm::pos_type r_cut = h*kernel_type::radius;

void
cuda_summation( thrust::host_vector<data_type>& host_data,
		thrust::host_vector<uint32_t> const& host_displs,
		uint16_t grid_i,
		uint16_t grid_j,
		uint16_t grid_k,
		swarm::pos_type delta_i,
		swarm::pos_type delta_j,
		swarm::pos_type delta_k );

application::application( int argc,
                          char* argv[] )
   : hpc::mpi::application( argc, argv )
{
   // LOG_PUSH( new hpc::logging::stdout( hpc::logging::trivial, "newton" ) );
   // LOG_PUSH( new hpc::logging::stdout( hpc::logging::debug ) );
   LOG_PUSH( new hpc::mpi::logger( "swarm.log.", hpc::logging::debug ) );
}

void
application::operator()()
{
   // Create the grid we will be using.
   grid_type grid(
      std::array<swarm::pos_type,n_dims>{ 0.0, 0.0, 0.0 },
      std::array<swarm::pos_type,n_dims>{ 10.0, 10.0, 10.0 },
      r_cut
      );

   // We're using a mapped population to begin with,
   // the most naive implementation.
   unsigned local_pop_size = pop_size;
   for( unsigned ii = 0; ii < n_dims; ++ii )
      local_pop_size = static_cast<double>( local_pop_size )*static_cast<double>( grid.local_sides()[ii] )/static_cast<double>( grid.global_sides()[ii] );
   LOGILN( "Local population size: ", local_pop_size );
   population_type pop( grid, local_pop_size );

   // Initialise the population to be uniform.
   hpc::engine.seed( hpc::mpi::comm::world.rank() );
   for( unsigned ii = 0; ii < pop.size(); ++ii )
   {
     auto& part = pop[ii];
     thrust::get<0>( part.data ) = hpc::generate_uniform<swarm::pos_type>( grid.local_min()[0], grid.local_max()[0] );
     thrust::get<1>( part.data ) = hpc::generate_uniform<swarm::pos_type>( grid.local_min()[1], grid.local_max()[1] );
     thrust::get<2>( part.data ) = hpc::generate_uniform<swarm::pos_type>( grid.local_min()[2], grid.local_max()[2] );
     thrust::get<n_dims>( part.data ) = 0.0;
     thrust::get<n_dims + 1>( part.data ) = h;
   }

   // Locate new population.
   pop.locate();

   // Density summation.
   hpc::profile::timer solve_time;
   {
      LOGBLOCKI( "Beginning summation." );
      auto ANON = solve_time.start();
      cuda_summation( pop.data(),
		      pop.displacements(),
		      grid.sides()[0], grid.sides()[1], grid.sides()[2],
		      grid.delta()[0],
		      grid.delta()[1],
		      grid.delta()[2] );
   }
   std::cout << "Solve time: " << solve_time.total() << " (s)\n";

   // // Dump results.
   // std::cout << thrust::get<3>( pop[0].data ) << ", " << thrust::get<4>( pop[0].data ) << "\n";
   // if( hpc::mpi::comm::world.rank() == 0 )
   //    dump_density_and_h( pop );
   // hpc::mpi::comm::world.barrier();
   // if( hpc::mpi::comm::world.rank() == 1 )
   //    dump_density_and_h( pop );
   // hpc::mpi::comm::world.barrier();
   // if( hpc::mpi::comm::world.rank() == 2 )
   //    dump_density_and_h( pop );
   // hpc::mpi::comm::world.barrier();
   // if( hpc::mpi::comm::world.rank() == 3 )
   //    dump_density_and_h( pop );
   // hpc::mpi::comm::world.barrier();
}
