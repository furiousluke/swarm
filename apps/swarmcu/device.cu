#include <boost/type_traits/integral_constant.hpp>
#include <thrust/device_vector.h>
#include <thrust/tuple.h>
#include <thrust/execution_policy.h>
#include <libhpc/algorithm/uniform.hh>
#include <swarm/kernel.hh>
#include <swarm/interaction.hh>
#include <swarm/types.hh>

struct data_type
{
   enum { n_dims = 3 };
   struct constant_h : boost::false_type {};
   struct massles    : boost::true_type  {};
   thrust::tuple<swarm::pos_type,swarm::pos_type,swarm::pos_type,swarm::density_type,swarm::h_type> data;
};

template< class Interaction,
	  class Kernel >
struct op
{
   op( data_type* data,
       uint32_t* displs,
       uint16_t grid_i,
       uint16_t grid_j,
       uint16_t grid_k,
       swarm::pos_type delta_i,
       swarm::pos_type delta_j,
       swarm::pos_type delta_k,
       swarm::h_type eta = 1.0 )
      : data( data ),
	displs( displs ),
	eta( eta )
   {
      grid_sides = hpc::make_array( grid_i, grid_j, grid_k );
      grid_delta = hpc::make_array( delta_i, delta_j, delta_k );
   }

   __device__ __host__
   void
   eval_kernel( uint p_i,
                data_type& d_i,
		uint p_j,
                data_type& d_j,
		swarm::pos_type r_ij_sq,
		swarm::h_type h_i )
   {
      hpc::array<swarm::h_type,2> val = kern.deriv_h( r_ij_sq, h_i );
      inter( d_i, d_j, 1.0, val[0] );
      deriv_h += val[1];
   }

   __device__ __host__
   void
   interaction( uint p_i,
                data_type& d_i,
		uint p_j,
		swarm::h_type h_i,
		swarm::pos_type r_cut )
   {
      swarm::pos3_type x_i = make_pos3( thrust::get<0>( d_i.data ),
      					thrust::get<1>( d_i.data ),
      					thrust::get<2>( d_i.data ) );
      data_type d_j = data[p_j];
      swarm::pos3_type x_j = make_pos3( thrust::get<0>( d_j.data ),
      					thrust::get<1>( d_j.data ),
      					thrust::get<2>( d_j.data ) );
      swarm::pos3_type x_ij = make_pos3( x_j.x - x_i.x, x_j.y - x_i.y, x_j.z - x_i.z );
      swarm::pos_type r_ij_sq = x_ij.x*x_ij.x + x_ij.y*x_ij.y + x_ij.z*x_ij.z;
      swarm::pos_type r_cut_sq = r_cut*r_cut;
      if( r_ij_sq < r_cut_sq )
      	 eval_kernel( p_i, d_i, p_j, d_j, r_ij_sq, h_i );
   }

   __device__ __host__
   void
   process_neighbor_cell( uint p_i,
			  data_type& d_i,
			  uint nbr,
			  swarm::h_type h_i,
			  swarm::pos_type r_cut )
   {
      uint part_begin = displs[nbr];
      uint part_end   = displs[nbr + 1];
      for( unsigned p_j = part_begin; p_j < part_end; ++p_j )
	 interaction( p_i, d_i, p_j, h_i, r_cut );
   }

   __device__ __host__
   void
   operator()( uint32_t cell )
   {
      // Get the grid origin for this cell.
      hpc::array<ushort,3> org = hpc::lift<uint32_t,ushort,3,ushort>( cell, grid_sides );

      // Calculate the particle begin and end positions.
      uint32_t part_begin = displs[cell];
      uint32_t part_end   = displs[cell + 1];

      // Track neighbor cell coordinate.
      hpc::array<ushort,3> nbr_crd;

      // Process all of the particles in this cell.
      for( unsigned p_i = part_begin; p_i < part_end; ++p_i )
      {
         // Cache some things for performance.
	 swarm::h_type h = thrust::get<4>( data[p_i].data );
         data_type d_i = data[p_i];

         // Run a Newton-Raphson solve.
	 swarm::h_type delta;
	 uint its = 0;
	 do
	 {
	    // Clear particle data before summation.
	    thrust::get<3>( d_i.data ) = 0.0;
	    deriv_h = 0.0;

            // Calculate grid metrics.
	    swarm::pos_type r_cut = h*(swarm::pos_type)Kernel::radius;
	    ushort3 step = make_ushort3( (ushort)(r_cut/grid_delta[0]) + 1,
					 (ushort)(r_cut/grid_delta[1]) + 1,
					 (ushort)(r_cut/grid_delta[2]) + 1 );
	    ushort3 low = make_ushort3( (org[0] > step.x) ? (org[0] - step.x) : 0,
					(org[1] > step.y) ? (org[1] - step.y) : 0,
					(org[2] > step.z) ? (org[2] - step.z) : 0 );
	    ushort3 upp = make_ushort3( (grid_sides[0] - org[0] > step.x) ? (org[0] + step.x + 1) : grid_sides[0],
					(grid_sides[1] - org[1] > step.y) ? (org[1] + step.y + 1) : grid_sides[1],
					(grid_sides[2] - org[2] > step.z) ? (org[2] + step.z + 1) : grid_sides[2] );

            // Iterate over neighboring grid cells.
	    for( nbr_crd[2] = low.z; nbr_crd[2] < upp.z; ++nbr_crd[2] )
	    {
	       for( nbr_crd[1] = low.y; nbr_crd[1] < upp.y; ++nbr_crd[1] )
	       {
		  for( nbr_crd[0] = low.x; nbr_crd[0] < upp.x; ++nbr_crd[0] )
		  {
                     // Project to the flattened grid index and process.
		     uint idx = hpc::project<ushort,uint,3,ushort>( nbr_crd, grid_sides );
		     process_neighbor_cell( p_i, d_i, idx, h, r_cut );
		  }
	       }
	    }

            // Claculate the Newton-Raphson values and check if we're done.
	    swarm::density_type rho = thrust::get<3>( d_i.data );
	    swarm::h_type new_h = eta*pow( 1.0/rho, 1.0/3.0 );
	    swarm::h_type f = h - new_h;
	    swarm::h_type dfdh = (eta*deriv_h*pow( 1.0/rho, 1.0/3.0 - 1.0 ))/(3.0*rho*rho) + 1.0;
	    delta = f/dfdh;
	    h -= delta;
	 }
	 while( fabs( delta ) > 1e-5 && ++its < 60 );

         // Update h and other data values.
         thrust::get<3>( data[p_i].data ) = thrust::get<3>( d_i.data );
	 thrust::get<4>( data[p_i].data ) = h;
      }
   }

   data_type* data;
   uint32_t* displs;
   hpc::array<ushort,3> grid_sides;
   hpc::array<swarm::pos_type,3> grid_delta;
   swarm::h_type deriv_h;
   swarm::h_type eta;
   Interaction inter;
   Kernel kern;
};

void
cuda_summation( thrust::host_vector<data_type>& host_data,
		thrust::host_vector<uint32_t> const& host_displs,
		uint16_t grid_i,
		uint16_t grid_j,
		uint16_t grid_k,
		swarm::pos_type delta_i,
		swarm::pos_type delta_j,
		swarm::pos_type delta_k )
{
   thrust::device_vector<data_type> data( host_data );
   thrust::device_vector<uint32_t> displs( host_displs );

   thrust::counting_iterator<uint32_t> cell_begin( 0 );
   thrust::counting_iterator<uint32_t> cell_end( host_displs.size() - 1 );

   thrust::for_each(
      cell_begin,
      cell_end,
      op< swarm::interactions::density<data_type>, swarm::kernels::cubic<3> >(
	 thrust::raw_pointer_cast( data.data() ),
	 thrust::raw_pointer_cast( displs.data() ),
	 grid_i, grid_j, grid_k,
	 delta_i, delta_j, delta_k
	 )
      );

   host_data = data;
}
