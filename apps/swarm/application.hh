#include <libhpc/mpi/application.hh>
#include <swarm/types.hh>

class application
   : public hpc::mpi::application
{
public:

   application( int argc,
                char* argv[] );

   void
   operator()();

protected:

   unsigned long long _pop_size;
   swarm::h_type _h;
   float _fac;
};
