#include <libhpc/containers/mymath.hh>
#include <libhpc/system/stream_output.hh>
#include <libhpc/profile/timer.hh>
#include <libhpc/mpi/logger.hh>
#include <swarm/swarm.hh>
#include "application.hh"

application::application( int argc,
                          char* argv[] )
   : hpc::mpi::application( argc, argv )
{
   options().add_options()
      ( "population,p", hpc::po::value<unsigned long long>( &_pop_size ), "Population size." )
      ( "smoothing-length,s", hpc::po::value<swarm::h_type>( &_h ), "Smoothing length." )
      ( "parallel-factor,f", hpc::po::value<float>( &_fac )->default_value( 0.0 ), "Parallel array factor." );
   parse_options( argc, argv );

   // LOG_PUSH( new hpc::logging::stdout( hpc::logging::trivial, "newton" ) );
   LOG_PUSH( new hpc::logging::stdout( hpc::logging::info ) );
   // LOG_PUSH( new hpc::mpi::logger( "swarm.log.", hpc::logging::debug ) );
}

void
application::operator()()
{
   // Setup constants and types.
   constexpr unsigned n_dims   = 3;
   constexpr swarm::h_type eta        = 1.2;
   typedef swarm::kernels::cubic<n_dims> kernel_type;
   typedef swarm::data::adaptive<n_dims>                           data_type;
   typedef swarm::mpi::grids::block<swarm::grids::uniform<n_dims>> grid_type;
   typedef swarm::populations::ordered<grid_type,data_type>        population_type;
   swarm::pos_type r_cut = _h*kernel_type::radius;

   // Create the grid we will be using.
   LOGILN( "Initial smoothing length: ", _h );
   grid_type grid(
      std::array<swarm::pos_type,n_dims>{ 0.0, 0.0, 0.0 },
      std::array<swarm::pos_type,n_dims>{ 10.0, 10.0, 10.0 },
      r_cut
      );
   LOGILN( "Grid size: ", grid.global_sides() );

   // We're using a mapped population to begin with,
   // the most naive implementation.
   population_type pop( grid, _fac, true ); // keep cells
   {
      LOGBLOCKI( "Initialising population." );
      LOGILN( "Global population size: ", _pop_size );
      swarm::populate::uniform( pop, _pop_size, _h );
      LOGILN( "Local population size: ", pop.size() );
   }

   // Locate new population.
   hpc::profile::timer locate_time;
   {
      LOGBLOCKI( "Locating population." );
      auto ANON = locate_time.start();
      pop.locate();
   }
   LOGILN( "Locate time: ", locate_time.total(), " (s)" );

   // Scatter population.
   hpc::profile::timer scatter_time;
   {
      LOGBLOCKI( "Scattering population." );
      auto ANON = scatter_time.start();
      pop.scatter<grid_type>();
   }
   LOGILN( "Scatter time: ", scatter_time.total(), " (s)" );

   // Density summation.
   hpc::profile::timer solve_time;
   {
      LOGBLOCKI( "Solving." );
      auto ANON = solve_time.start();
      typedef swarm::interactions::density<data_type> inter_type;
      swarm::summation<population_type> sum( eta );
      sum( pop, inter_type(), kernel_type() );
   }
   LOGILN( "Solve time: ", solve_time.total(), " (s)\n" );

   dump_density_and_h( pop );
}
