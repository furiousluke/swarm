#include <stdint.h>
#include <boost/type_traits/integral_constant.hpp>
#include <thrust/tuple.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <swarm/types.hh>

struct data_type
{
   enum { n_dims = 3 };
   struct constant_h : boost::false_type {};
   struct massles    : boost::true_type  {};
   thrust::tuple<swarm::pos_type,swarm::pos_type,swarm::pos_type,swarm::density_type,swarm::h_type> data;
};

void
cuda_sort( thrust::host_vector<data_type>& data,
	   thrust::host_vector<uint32_t>& cells )
{
   thrust::device_vector<data_type> dev_data( data );
   thrust::device_vector<uint32_t> dev_cells( cells );
   thrust::sort_by_key( dev_cells.begin(), dev_cells.end(), dev_data.begin() );
   data = dev_data;
   cells = dev_cells;
}
