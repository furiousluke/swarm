// #define USE_CUDA_SORT

#include <boost/type_traits/integral_constant.hpp>
#include <thrust/host_vector.h>
#include <thrust/tuple.h>
#include <libhpc/containers/mymath.hh>
#include <libhpc/system/stream_output.hh>
#include <libhpc/profile/timer.hh>
#include <libhpc/mpi/logger.hh>
#include <libhpc/mpi/cuda.hh>
#include <swarm/types.hh>

struct data_type
{
   enum { n_dims = 3 };
   struct constant_h : boost::false_type {};
   struct massles    : boost::true_type  {};
   thrust::tuple<swarm::pos_type,swarm::pos_type,swarm::pos_type,swarm::density_type,swarm::h_type> data;
};

#include <swarm/swarm.hh>
#include "application.hh"

constexpr unsigned      n_dims = 3;
constexpr swarm::h_type eta    = 1.2;

typedef swarm::kernels::cubic<n_dims>                                        kernel_type;
//typedef swarm::mpi::grids::block<swarm::grids::uniform<n_dims>>              grid_type;
typedef swarm::grids::uniform<n_dims>                                        grid_type;
typedef swarm::populations::ordered<grid_type,data_type,thrust::host_vector> population_type;
typedef swarm::interactions::density<data_type>                              interaction_type;

void
cuda_summation( thrust::host_vector<data_type>& host_data,
		thrust::host_vector<swarm::real_type>& host_v,
		thrust::host_vector<uint32_t> const& host_displs,
		thrust::host_vector<uint32_t> const& cells,
		uint16_t grid_i,
		uint16_t grid_j,
		uint16_t grid_k,
		swarm::pos_type delta_i,
		swarm::pos_type delta_j,
		swarm::pos_type delta_k,
		swarm::h_type eta,
		bool first );

application::application( int argc,
                          char* argv[] )
   : hpc::mpi::application( argc, argv )
{
   options().add_options()
      ( "population,p", hpc::po::value<unsigned long long>( &_pop_size ), "Population size." )
      ( "smoothing-length,s", hpc::po::value<swarm::h_type>( &_h ), "Smoothing length." )
      ( "parallel-factor,f", hpc::po::value<float>( &_fac )->default_value( 0.0 ), "Parallel array factor." );
   parse_options( argc, argv );

   // LOG_PUSH( new hpc::logging::stdout( hpc::logging::trivial, "newton" ) );
   LOG_PUSH( new hpc::logging::stdout( hpc::logging::info ) );
   // LOG_PUSH( new hpc::mpi::logger( "swarm.log.", hpc::logging::info ) );
}

void
application::operator()()
{
   // Select appropriate CUDA device.
   hpc::cuda::select_device();

   // Dump available device memory.
   {
      size_t avail, free;
      INSIST( cudaMemGetInfo( &free, &avail ), == cudaSuccess );
      LOGILN( "Device memory: ", 0.000001*(double)free, "/", 0.000001*(double)avail, " GB" );
   }

   // Create the grid we will be using.
   LOGILN( "Initial smoothing length: ", _h );
   swarm::pos_type r_cut = _h*kernel_type::radius;
   grid_type grid(
      std::array<swarm::pos_type,n_dims>{ 0.0, 0.0, 0.0 },
      std::array<swarm::pos_type,n_dims>{ 10.0, 10.0, 10.0 },
      r_cut
      );
   LOGILN( "Domain grid size: ", grid.sides() );

   // We're using a mapped population to begin with,
   // the most naive implementation.
   population_type pop( grid, _fac, true ); // keep cells
   {
      LOGBLOCKI( "Initialising population." );
      LOGILN( "Global population size: ", _pop_size );
      swarm::populate::uniform( pop, _pop_size, _h );
      LOGILN( "Local population size: ", pop.size() );
   }

   // Do a few iterations.
   unsigned step = 0, n_steps = 10000;
   while( step <= n_steps )
   {
      LOGBLOCKI( "Step ", step, " of ", n_steps, "." );

      // If we have moved, update the grid.
      if( step > 1 )
      {
	 pop.update();
	 r_cut = pop.mean_h()*kernel_type::radius;
	 grid.reset( pop.min(), pop.max(), r_cut );
      }

      // Locate population.
      hpc::profile::timer locate_time;
      {
	 LOGBLOCKI( "Locating population." );
	 auto ANON = locate_time.start();
	 pop.locate();
      }
      LOGILN( "Locate time: ", locate_time.total(), " (s)" );

      // // Scatter population.
      // hpc::profile::timer scatter_time;
      // {
      // 	 LOGBLOCKI( "Scattering population." );
      // 	 auto ANON = scatter_time.start();
      // 	 pop.scatter<grid_type>();
      // 	 LOGILN( "Domain particles: ", pop.size() );
      // }
      // LOGILN( "Scatter time: ", scatter_time.total(), " (s)" );

      // Density summation.
      thrust::host_vector<swarm::real_type> v;
      std::fill( v.begin(), v.end(), 0.0 );
      hpc::profile::timer solve_time;
      {
	 LOGBLOCKI( "Solving." );
	 auto ANON = solve_time.start();
	 cuda_summation( pop.data(),
			 v,
			 pop.displacements(),
			 pop.cells(),
			 grid.sides()[0], grid.sides()[1], grid.sides()[2],
			 grid.delta()[0],
			 grid.delta()[1],
			 grid.delta()[2],
			 eta,
			 step == 0 );
      }
      LOGILN( "Solve time: ", solve_time.total(), " (s)" );

      hpc::array<swarm::real_type,n_dims> avg_v, max_v, min_v;
      for( unsigned jj = 0; jj < n_dims; ++jj )
      {
	 min_v[jj] = std::numeric_limits<swarm::real_type>::max();
	 max_v[jj] = std::numeric_limits<swarm::real_type>::min();
      }
      for( unsigned ii = 0; ii < pop.size(); ++ii )
      {
	 for( unsigned jj = 0; jj < n_dims; ++jj )
	 {
	    min_v[jj] = std::min<swarm::real_type>( v[n_dims*ii + jj], min_v[jj] );
	    max_v[jj] = std::max<swarm::real_type>( v[n_dims*ii + jj], max_v[jj] );
	    avg_v[jj] += v[n_dims*ii + jj];
	 }
      }
      for( unsigned jj = 0; jj < n_dims; ++jj )
	 avg_v[jj] /= (swarm::real_type)pop.size();
      LOGILN( "Min v: ", min_v );
      LOGILN( "Max v: ", max_v );
      LOGILN( "Mean v: ", avg_v );

      if( step%100 == 0 )
	 swarm::write_xdmf( pop, std::string( "output." ) + hpc::to_string( step ) );
      ++step;
   }
}
