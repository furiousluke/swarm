#include <boost/type_traits/integral_constant.hpp>
#include <thrust/device_vector.h>
#include <thrust/tuple.h>
#include <thrust/execution_policy.h>
#include <libhpc/profile/timer.hh>
#include <swarm/kernel.hh>
#include <swarm/transfer.hh>
#include <swarm/cuda/summation.hh>
#include <swarm/cuda/interaction.hh>
#include <swarm/equation_of_state.hh>
#include <swarm/types.hh>

struct data_type
{
   enum { n_dims = 3 };
   struct constant_h : boost::false_type {};
   struct massles    : boost::true_type  {};
   thrust::tuple<swarm::pos_type,swarm::pos_type,swarm::pos_type,swarm::density_type,swarm::h_type> data;
};

void
cuda_summation( thrust::host_vector<data_type>& host_data,
		thrust::host_vector<swarm::real_type>& host_v,
		thrust::host_vector<uint32_t> const& host_displs,
		thrust::host_vector<uint32_t> const& host_cells,
		uint16_t grid_i,
		uint16_t grid_j,
		uint16_t grid_k,
		swarm::real_type delta_i,
		swarm::real_type delta_j,
		swarm::real_type delta_k,
		swarm::real_type eta,
		bool first )
{
   typedef swarm::eos::ideal<swarm::real_type>               eos_type;
   typedef swarm::kernels::cubic<3>                          kernel_type;
   typedef swarm::cuda::pressure_force<kernel_type,eos_type> update_type;

   thrust::device_vector<swarm::real_type> x;
   thrust::device_vector<swarm::real_type> v;
   thrust::device_vector<swarm::real_type> h;
   thrust::device_vector<swarm::real_type> rho;
   thrust::device_vector<swarm::real_type> omega;
   thrust::device_vector<uint32_t>         displs;
   thrust::device_vector<uint32_t>         cells;
   hpc::profile::timer trans_time;
   {
      hpc::profile::timer_handle ANON = trans_time.start();
      rho.resize( host_data.size() );
      omega.resize( host_data.size() );
      v.resize( 3*host_data.size() );
      displs = host_displs;
      cells = host_cells;
      swarm::htd_x_h( host_data, x, h );
      cudaDeviceSynchronize();
   }

   thrust::counting_iterator<uint64_t> part_begin( 0 );
   thrust::counting_iterator<uint64_t> part_end( host_data.size() );

   hpc::profile::timer solve_time;
   hpc::profile::timer density_time;
   hpc::profile::timer momentum_time;
   {
      hpc::profile::timer_handle ANON = solve_time.start();
      {
	 hpc::profile::timer_handle ANON = density_time.start();
	 thrust::for_each(
	    part_begin,
	    part_end,
	    swarm::cuda::density_summation< swarm::cuda::density< swarm::kernels::cubic<3> > >(
	       swarm::cuda::density< swarm::kernels::cubic<3> >(),
	       thrust::raw_pointer_cast( x.data() ),
	       thrust::raw_pointer_cast( h.data() ),
	       thrust::raw_pointer_cast( rho.data() ),
	       thrust::raw_pointer_cast( omega.data() ),
	       thrust::raw_pointer_cast( displs.data() ),
	       thrust::raw_pointer_cast( cells.data() ),
	       grid_i, grid_j, grid_k,
	       delta_i, delta_j, delta_k,
	       eta
	       )
	    );
	 cudaDeviceSynchronize();
      }

      {
      	 hpc::profile::timer_handle ANON = momentum_time.start();
      	 thrust::for_each(
      	    part_begin,
      	    part_end,
      	    swarm::cuda::summation<update_type>(
	       update_type( thrust::raw_pointer_cast( v.data() ), first ),
      	       thrust::raw_pointer_cast( x.data() ),
      	       thrust::raw_pointer_cast( h.data() ),
      	       thrust::raw_pointer_cast( rho.data() ),
      	       thrust::raw_pointer_cast( omega.data() ),
      	       thrust::raw_pointer_cast( displs.data() ),
      	       thrust::raw_pointer_cast( cells.data() ),
      	       grid_i, grid_j, grid_k,
      	       delta_i, delta_j, delta_k,
      	       eta
      	       )
      	    );
      	 cudaDeviceSynchronize();
      }
   }

   {
      hpc::profile::timer_handle ANON = trans_time.start();
      swarm::dth_x_v_h_rho( host_data, host_v, x, v, h, rho );
      cudaDeviceSynchronize();
   }

   std::cout << "  Transfer time: " << trans_time.total() << " (s)\n";
   std::cout << "  Solve time: " << solve_time.total() << " (s)\n";
   std::cout << "    Density time: " << density_time.total() << " (s)\n";
   std::cout << "    Momentum time: " << momentum_time.total() << " (s)\n";
}
