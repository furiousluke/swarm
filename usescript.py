##
## libhpc build script. The following are automatially added to
## the local scope:
##
##   files     - A package including basic file operations.
##   platform  - A package including platform specific routines.
##

##
## Define configuration/build arguments.
##

args = (arguments()
        ('--prefix', default='/usr/local', help='Installation path.')
        ('--enable-debug', dest='debug', action='boolean', default=False, help='Enable/disable debugging mode.')
        ('--enable-openmp', dest='openmp', action='boolean', default=False, help='Enable/disable OpenMP.')
        ('--enable-instrument', dest='instrument', action='boolean', default=False, help='Enable/disable instrumentation.')
        ('--enable-stacktrace', dest='stacktrace', action='boolean', default=False, help='Enable/disable debugging stacktrace.')
        ('--enable-memory-debug', dest='memory_debug', action='boolean', default=False, help='Enable/disable memory debugging.')
        ('--enable-memory-ops', dest='memory_ops', action='boolean', default=False, help='Enable/disable memory operation logging.')
        ('--enable-memory-stats', dest='memory_stats', action='boolean', default=False, help='Enable/disable memory statistics logging.')
        ('--enable-logging', dest='logging', action='boolean', default=True, help='Enable/disable all logging routines.'))

##
## Declare the packages we are interested in.
##

glut    = use('glut')
hdf5    = use('hdf5')
boost   = use('boost')
mpi     = use('mpi')
dl      = use('dl')
cuda    = use('cuda')
thrust  = use('thrust')

##
## Declare option sets.
##

# C++ compiler options.
cc_opts = (

    # General options.
    options(define = [platform.os_name.upper()]) + 

    # Debug mode.
    options(args.debug == True,
            prefix       = 'build/debug',
            library_dirs = ['build/debug/lib'],
            rpath_dirs   = ['build/debug/lib'],
            header_dirs  = ['build/debug/include', 'build/debug/include/swarm'],
            optimise     = 0,
            symbols      = True) +

    # Optimised mode.
    options(args.debug == False,
            prefix       = 'build/optimised',
            library_dirs = ['build/optimised/lib'],
            rpath_dirs   = ['build/optimised/lib'],
            header_dirs  = ['build/optimised/include', 'build/optimised/include/swarm'],
            optimise     = 3,
            symbols      = False,
            define       = ['NDEBUG', 'NLOGTRIVIAL', 'NLOGDEBUG']) +

    # Optional compilation/linking commands.
    options(args.instrument   == False, define=['NINSTRUMENT']) +
    options(args.logging      == False, define=['NLOG']) +
    options(args.stacktrace   == False, define=['NSTACKTRACE']) +
    options(args.memory_debug == False, define=['NMEMDEBUG']) +
    options(args.memory_ops   == False, define=['NMEMOPS']) +
    options(args.memory_stats == False, define=['NMEMSTATS']) +

    # Optional packages.
    options(args.openmp                  == True, openmp=True) + 
    options(glut.have                    == True, define=['HAVE_GLUT']) +
    options(cuda.have                    == True, define=['HAVE_CUDA']) +
    options(thrust.have                  == True, define=['HAVE_THRUST']) +
    options(hdf5.has_feature('parallel') == True, define=['PARALLELHDF5'])
)

# Copy/install options.
cp_opts = (
    options(args.debug == True,
            prefix='build/debug/include/swarm') +
    options(args.debug == False,
            prefix='build/optimised/include/swarm')
)

##
## Define compilers/linkers, or anything dependant on the options.
##

cc        =           use('cuda', cc_opts,                              compile=True)
bin       =           use('cuda', cc_opts)
# nvcc      =           use('cuda',         cc_opts,                              compile=True)
ar        =           use('ar',           cc_opts,                              add=True)
cp        = files.feature('copy',         cp_opts)
hdr_inst  = files.feature('copy',         None,    targets.contains('install'), prefix=args.prefix + '/include/swarm')
lib_inst  = files.feature('copy',         None,    targets.contains('install'), prefix=args.prefix)
run_tests = files.feature('run',          None,    targets.contains('check'))
pkgs      = dl + boost + mpi + hdf5 + (glut | identity) + thrust + cuda
cc        = cc  + pkgs
bin       = bin + pkgs

##
## Declare build rules.
##

hdrs       = rule(r'src/.+\.(?:hh|hpp|tcc)$', cp & hdr_inst, target_strip_dirs=1)
objs       = rule(r'src/.+\.(?:cc|hcc)$',     cc)
static_lib = rule(objs,                       ar,            target=platform.make_static_library('lib/swarm'))
dummy      = rule(static_lib,                 lib_inst,      target_strip_dirs=2)
tests      = rule(r'tests/.+\.(?:cc|cu)$',    bin,           libraries=['swarm'], single=False, suffix='')
dummy      = rule(tests,                      run_tests,     target=dummies.always)

# # Applications.
# rule(r'apps/swarm/.+\.cc$', bin, target='bin/swarm', libraries=['swarm'])
# rule(r'exs/libhpc/mpi/host.cc', bin, target='bin/host', libraries=['swarm'])

# # use_cuda = (cuda.have == True) & (thrust.have == True)
# swarmcu_objs = rule(r'apps/swarmcu/.+\.cc$', cc) #, use_cuda)
# swarmcu_cus = rule(r'apps/swarmcu/.+\.cu$', nvcc) #, use_cuda)
# # rule(swarmcu_objs + swarmcu_cus, bin, None, target='bin/swarmcu', libraries=['swarm'])
# rule(swarmcu_objs + swarmcu_cus, bin + cuda, target='bin/swarmcu', libraries=['swarm'])

# swarmcub_objs = rule(r'apps/swarmcub/.+\.cc$', cc) #, use_cuda)
# swarmcub_cus = rule(r'apps/swarmcub/.+\.cu$', nvcc) #, use_cuda)
# rule(swarmcub_objs + swarmcub_cus, bin + cuda, target='bin/swarmcub', libraries=['swarm'])

# swarmcuc_objs = rule(r'apps/swarmcuc/.+\.cc$', cc) #, use_cuda)
# swarmcuc_cus = rule(r'apps/swarmcuc/.+\.cu$', nvcc) #, use_cuda)
# rule(swarmcuc_objs + swarmcuc_cus, bin + cuda, target='bin/swarmcuc', libraries=['swarm'])
