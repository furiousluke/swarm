#ifndef swarm_summation_hh
#define swarm_summation_hh

#include <cmath>
#include <libhpc/logging/logging.hh>
#include <libhpc/system/stream_output.hh>
#include <libhpc/containers/richardson.hh>
#include <libhpc/containers/newton.hh>
#include <libhpc/containers/num.hh>
#include "newton.hh"

namespace swarm {

   template< class Population >
   class summation_base
   {
   public:

      typedef          Population              population_type;
      typedef typename Population::grid_type   grid_type;
      typedef typename Population::data_type   data_type;
      static constexpr unsigned                n_dims = grid_type::n_dims;

   public:

      summation_base( h_type eta = 1.2 )
	 : _pop( nullptr ),
	   _grid( nullptr ),
	   _eta( eta )
      {
      }

      summation_base( population_type& pop,
		      h_type eta = 1.2 )
	 : _pop( &pop ),
	   _grid( pop.grid() ),
	   _eta( eta )
      {
      }

      template< class Interaction,
                class Kernel >
      void
      sum_particle( Interaction const& inter,
                    Kernel const& kern,
                    uint32_t cell,
                    h_type h,
                    pos_type r_cut,
                    data_type& data_i )
      {
         _n_nbr_cells = 0;
         _n_nbr_parts = 0;

         // Clear any values.
         clear_particle( data_i );

         // Iterate over all neighboring grid cells.
         for( auto c_it = _grid->begin_neighbors( cell, r_cut ); c_it != _grid->end_neighbors( cell, r_cut ); ++c_it )
         {
            // Don't process a cell if it is empty.
            if( !_pop->cell_size( *c_it ) )
               continue;
            LOGBLOCKT( "Processing neighboring cell: ", *c_it );
            ++_n_nbr_cells;

            // Process particles in neighboring cells.
            for( auto n_it = _pop->begin_cell( *c_it ); n_it != _pop->end_cell( *c_it ); ++n_it )
            {
               LOGBLOCKT( "Interaction with: ", n_it.index() );
               interaction( inter, kern, data_i, *n_it, h, r_cut );
            }
         }
      }

      template< class Data >
      typename std::enable_if<Data::constant_h::value>::type
      clear_particle( Data& data_i )
      {
         std::get<n_dims>( data_i.data ) = 0.0;
      }

      template< class Data >
      typename std::enable_if<!Data::constant_h::value>::type
      clear_particle( Data& data_i )
      {
         std::get<n_dims>( data_i.data ) = 0.0;
         _deriv_h = 0.0;
      }

      template< class Interaction,
		class Kernel >
      typename std::enable_if<Kernel::n_dims == 2>::type
      interaction( Interaction const& inter,
		   Kernel const& kern,
		   data_type& data_i,
                   data_type& data_j,
                   h_type h_i,
		   pos_type r_cut )
      {
         std::array<pos_type,2> x_i{ std::get<0>( data_i.data ), std::get<1>( data_i.data ) };
         std::array<pos_type,2> x_j{ std::get<0>( data_j.data ), std::get<1>( data_j.data ) };
         std::array<pos_type,2> x_ij{ x_j[0] - x_i[0], x_j[1] - x_i[1] };
         pos_type r_ij_sq = x_ij[0]*x_ij[0] + x_ij[1]*x_ij[1];
         LOGTLN( "Position i: ", x_i );
         LOGTLN( "Position j: ", x_j );
         LOGTLN( "Distance: ", sqrt( r_ij_sq ) );
	 pos_type r_cut_sq = r_cut*r_cut;
         if( r_ij_sq < r_cut_sq )
	 {
	    LOGTLN( "Within cut-off radius: ", r_cut );
            eval_kernel( inter, kern, data_i, data_j, r_ij_sq, h_i );
            ++_n_nbr_parts;
	 }
      }

      template< class Interaction,
		class Kernel >
      typename std::enable_if<Kernel::n_dims == 3>::type
      interaction( Interaction const& inter,
		   Kernel const& kern,
		   data_type& data_i,
                   data_type& data_j,
                   h_type h_i,
		   pos_type r_cut )
      {
         std::array<pos_type,3> x_i{ std::get<0>( data_i.data ), std::get<1>( data_i.data ), std::get<2>( data_i.data ) };
         std::array<pos_type,3> x_j{ std::get<0>( data_j.data ), std::get<1>( data_j.data ), std::get<2>( data_j.data ) };
         std::array<pos_type,3> x_ij{ x_j[0] - x_i[0], x_j[1] - x_i[1], x_j[2] - x_i[2] };
         pos_type r_ij_sq = x_ij[0]*x_ij[0] + x_ij[1]*x_ij[1] + x_ij[2]*x_ij[2];
         LOGTLN( "Position i: ", x_i );
         LOGTLN( "Position j: ", x_j );
         LOGTLN( "Distance: ", sqrt( r_ij_sq ) );
	 pos_type r_cut_sq = r_cut*r_cut;
         if( r_ij_sq < r_cut_sq )
	 {
	    LOGTLN( "Within cut-off radius: ", r_cut );
            eval_kernel( inter, kern, data_i, data_j, r_ij_sq, h_i );
            ++_n_nbr_parts;
	 }
      }

      template< class Interaction,
                class Kernel,
                class Data >
      typename std::enable_if<Data::constant_h::value,unsigned>::type
      eval_kernel( Interaction const& inter,
                   Kernel const& kern,
                   Data& data_i,
                   Data& data_j,
                   pos_type r_ij_sq,
                   h_type h_i )
      {
         inter( data_i, data_j, 1.0, kern( r_ij_sq, h_i ) );
      }

      template< class Interaction,
                class Kernel,
                class Data >
      typename std::enable_if<!Data::constant_h::value,unsigned>::type
      eval_kernel( Interaction const& inter,
                   Kernel const& kern,
                   Data& data_i,
                   Data& data_j,
                   pos_type r_ij_sq,
                   h_type h_i )
      {
         auto val = kern.deriv_h( r_ij_sq, h_i );
         inter( data_i, data_j, 1.0, val[0] );
         _deriv_h += val[1];
      }

      h_type
      deriv_h() const
      {
	 return _deriv_h;
      }

      unsigned
      n_nbr_cells() const
      {
         return _n_nbr_cells;
      }

      unsigned
      n_nbr_particles() const
      {
         return _n_nbr_parts;
      }

   protected:

      population_type* _pop;
      grid_type const* _grid;
      h_type _deriv_h;
      h_type _eta;
      unsigned _n_nbr_cells;
      unsigned _n_nbr_parts;
   };

   template< class Population,
             class Enable = void >
   class summation
      : public summation_base<Population>
   {
   public:

      typedef          summation<Population,Enable> this_type;
      typedef          summation_base<Population>   super_type;
      typedef          Population              population_type;
      typedef typename Population::grid_type   grid_type;
      typedef typename Population::data_type   data_type;
      static constexpr unsigned                n_dims = grid_type::n_dims;

   public:

      summation( h_type eta = 1.2 )
	 : super_type( eta )
      {
      }

      summation( population_type& pop,
		 h_type eta = 1.2 )
	 : super_type( pop, eta )
      {
      }

      void
      set_population( population_type& pop )
      {
	 this->_pop = &pop;
	 this->_grid = pop.grid();
      }

      population_type const*
      population() const
      {
         return this->_pop;
      }

      grid_type const*
      grid() const
      {
         return this->_grid;
      }

      template< class Interaction,
        	class Kernel >
      void
      operator()( population_type& pop,
        	  Interaction const& inter,
        	  Kernel const& kern )
      {
         // Cache objects.
	 set_population( pop );

         // Create the function.
         swarm::newton_function<this_type,Interaction,Kernel> func( *this, inter, kern, 0, nullptr, this->_eta );

         // Iterate over all grid cells.
	 LOGILN( "Using eta: ", this->_eta );
	 LOGILN( "Cells to process: ", this->_grid->n_cells() );
         for( uint32_t ii = 0; ii < this->_grid->n_cells(); ++ii )
         {
	    // Keep track of mean h.
	    h_type mean_h = 0.0;
	    density_type mean_rho = 0.0;
            float mean_its = 0.0;
            float mean_nbrs = 0.0;

            // Don't process a cell if it is empty.
            if( !pop.cell_size( ii ) )
               continue;
            LOGBLOCKI( "Processing cell: ", ii );

            // Update the function.
            func.set_cell( ii );

            // Iterate over every particle in the root cell.
            for( auto p_it = pop.begin_cell( ii ); p_it != pop.end_cell( ii ); ++p_it )
            {
               LOGBLOCKD( "Processing particle: ", p_it.index() );
               auto& data_i = *p_it;

               // Update the function with current value.
               func.set_data( data_i );

               // Use the Newton-Raphson method augmented with bisection to find the answer.
	       h_type h = std::get<n_dims + 1>( data_i.data );
               unsigned its;
               std::tie( h, its ) = hpc::num::newton_its<decltype(func),h_type>( func, 1e-5, 100.0, h, 1e-5 );

	       // Check that it worked.
	       ASSERT( hpc::num::approx<h_type>( h, this->_eta*pow( 1.0/std::get<n_dims>( data_i.data ), 1.0/static_cast<h_type>( n_dims ) ), 1e-5 ),
	       	       "Smoothing length not resolved." );

               // Update new h.
               std::get<n_dims + 1>( data_i.data ) = h;
	       mean_h += h;
	       mean_rho += std::get<n_dims>( data_i.data );
               mean_its += static_cast<float>( its );
               mean_nbrs += this->n_nbr_particles();
            }

	    // Dump means.
	    mean_h /= static_cast<h_type>( this->_pop->cell_size( ii ) );
	    mean_rho /= static_cast<density_type>( this->_pop->cell_size( ii ) );
            mean_its /= static_cast<float>( this->_pop->cell_size( ii ) );
            mean_nbrs /= static_cast<float>( this->_pop->cell_size( ii ) );
	    LOGILN( "Mean h    = ", mean_h );
	    LOGILN( "Mean rho  = ", mean_rho );
            LOGILN( "Mean its  = ", mean_its );
            LOGILN( "Mean nbrs = ", mean_nbrs );
         }
      }
   };

   template< class Population >
   class summation< Population,
                    typename std::enable_if<Population::data_type::constant_h::value>::type >
      : public summation_base<Population>
   {
   public:

      typedef          Population              population_type;
      typedef typename Population::grid_type   grid_type;
      typedef typename Population::data_type   data_type;
      static constexpr unsigned                n_dims = grid_type::n_dims;

   public:

      template< class Interaction,
		class Kernel >
      void
      operator()( population_type& pop,
		  Interaction const& inter,
		  Kernel const& kern )
      {
         // Cache objects.
         this->_pop = &pop;
         this->_grid = pop.grid();

	 // Constant smoothing length means no need to recalculate
	 // cutoff radius.
	 h_type h = pop.smoothing_length();
	 pos_type r_cut = h*Kernel::radius;
	 LOGDLN( "Using cutoff radius: ", r_cut );

	 // Iterate over all grid cells.
         for( uint32_t ii = 0; ii < this->_grid->n_cells(); ++ii )
         {
            // Don't process a cell if it is empty.
            if( !pop.cell_size( ii ) )
               continue;
	    LOGBLOCKD( "Processing cell: ", ii );

            // Iterate over every particle in the root cell.
            for( auto p_it = pop.begin_cell( ii ); p_it != pop.end_cell( ii ); ++p_it )
            {
               LOGBLOCKT( "Processing particle: ", p_it.index() );
               this->sum_particle( inter, kern, ii, h, r_cut, *p_it );
            }
         }
      }
   };

}

#endif
