#ifndef swarm_kernel_hh
#define swarm_kernel_hh

#include <cmath>
#include <boost/array.hpp>
#include <libhpc/system/cuda.hh>
#include "types.hh"
#include "math.hh"

namespace swarm {
   namespace kernels {

      template< unsigned D >
      class cubic
      {
      public:

#ifndef __CUDACC__
         static constexpr real_type n_dims    = D;
         static constexpr real_type radius    = 2.0;
         static constexpr real_type radius_sq = 4.0;
         static constexpr real_type norm[4]   = { 0.0,
                                                  2.0/3.0,
                                                  10.0/(7.0*M_PI),
                                                  1.0/M_PI };
#else
         enum { n_dims    = D };
         enum { radius    = 2 };
         enum { radius_sq = 4 };
#endif

      public:

         CUDA_DEV_HOST
         real_type
         operator()( real_type r_sq,
                     real_type h ) const
         {
	    real_type q = swarm::sqrt<real_type>( r_sq )/h;
            if( q < 1.0 )
	       return _norm<D>()*(0.25*swarm::pow<real_type>( 2.0 - q, 3.0 ) - swarm::pow<real_type>( 1.0 - q, 3.0 ))/swarm::pow<real_type>( h, (real_type)D );
            else if( q < 2.0 )
               return _norm<D>()*0.25*swarm::pow<real_type>( 2.0 - q, 3.0 )/swarm::pow<real_type>( h, (real_type)D );
            else
               return 0.0;
         }

         CUDA_DEV_HOST
         real_type
         deriv( real_type r_sq,
		real_type h ) const
         {
	    real_type q = swarm::sqrt<real_type>( r_sq )/h;
            if( q < 1.0 )
	       return _norm<D>()*(-3.0*q + 2.25*q*q)/swarm::pow<real_type>( h, (real_type)(D + 1) );
            else if( q < 2.0 )
               return _norm<D>()*(0.75*q*q + 3.0*q - 3.0)/swarm::pow<real_type>( h, (real_type)(D + 1) );
            else
               return 0.0;
         }

         CUDA_DEV_HOST
	 boost::array<real_type,2>
         deriv_h( real_type r_sq,
                  real_type h ) const
         {
            real_type r = swarm::sqrt<real_type>( r_sq );
	    real_type q = r/h;
            real_type h_sq = h*h;
            real_type h_sq_inv = 1.0/h_sq;
            real_type W, dWdh;
            real_type pow_d_inv = 1.0/swarm::pow<real_type>( h, (real_type)D );
            if( q < 1.0 )
            {
               real_type enm = 0.25*swarm::pow<real_type>( 2.0 - q, 3.0 ) - swarm::pow<real_type>( 1.0 - q, 3.0 );
               W = _norm<D>()*enm*pow_d_inv;
               real_type n = _norm<D>();
               real_type d = n_dims;
               dWdh = -(n*((3.0*r*swarm::pow<real_type>( q - 1.0, 2.0 ))*h_sq_inv - (3.0*r*swarm::pow<real_type>( q - 2.0, 2.0 ))/(4.0*h_sq)))/swarm::pow<real_type>( h, d ) -
                  (d*n*(swarm::pow<real_type>( q - 1.0, 3.0 ) - swarm::pow<real_type>( q - 2.0, 3.0 )/4.0))/swarm::pow<real_type>( h, d + 1.0 );
            }
            else if( q < 2.0 )
            {
               real_type enm = swarm::pow<real_type>( 2.0 - q, 3.0 );
               W = _norm<D>()*0.25*enm*pow_d_inv;
               real_type n = _norm<D>();
               real_type d = n_dims;
               dWdh = (d*n*swarm::pow<real_type>( q - 2.0, 3.0 ))/(4.0*swarm::pow<real_type>( h, d + 1.0 )) +
                  (3.0*n*r*swarm::pow<real_type>( q - 2.0, 2.0 ))/(4.0*h*h*swarm::pow<real_type>( h, d ));
            }
            else
            {
               W = 0.0;
               dWdh = 0.0;
            }
	    return boost::array<real_type,2>{ W, dWdh };
         }

#ifndef __CUDACC__
         template< unsigned E >
         inline
         real_type
         _norm() const
         {
            return norm[E];
         }
#else
         template< unsigned E >
         inline
         real_type
         _norm() const;

         template<>
         inline
         real_type
         _norm<1>() const
         {
            return 2.0/3.0;
         }

         template<>
         inline
         real_type
         _norm<2>() const
         {
            return 10.0/(7.0*M_PI);
         }

         template<>
         inline
         real_type
         _norm<3>() const
         {
            return 1.0/M_PI;
         }
#endif
      };

   }
}

#endif
