#ifndef swarm_math_hh
#define swarm_math_hh

#include <libhpc/system/cuda.hh>

namespace swarm {

   template< class T >
   T
   pow( T x,
	T y );

   template<>
   CUDA_DEV_HOST_INL
   float
   pow<float>( float x,
	       float y )
   {
      return powf( x, y );
   }

   template<>
   CUDA_DEV_HOST_INL
   double
   pow<double>( double x,
		double y )
   {
      return ::pow( x, y );
   }

   template< class T >
   T
   sqrt( T x );

   template<>
   CUDA_DEV_HOST_INL
   float
   sqrt<float>( float x )
   {
      return sqrtf( x );
   }

   template<>
   CUDA_DEV_HOST_INL
   double
   sqrt<double>( double x )
   {
      return ::sqrt( x );
   }

}

#endif
