#ifndef swarm_grid_hh
#define swarm_grid_hh

#include <boost/array.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <libhpc/debug/assert.hh>
#include <libhpc/algorithm/uniform.hh>
#include <libhpc/algorithm/morton.hh>
#include <libhpc/system/math.hh>
#include <libhpc/system/cuda.hh>
#include "neighbors.hh"
#include "types.hh"

namespace swarm {

   ///
   /// Grid base.
   ///
   template< unsigned D,
	     class Final >
   class grid
   {
   public:

      typedef neighbor_cell_iterator<D,Final> neighbor_iterator;
#ifdef CXX_0X
      static constexpr unsigned n_dims_type = D;
      static constexpr bool is_parallel_type = false;
#else
      enum { n_dims_type = D };
      enum { is_parallel_type = 0 };
#endif

   public:

      CUDA_DEV_HOST
      grid()
         : _n_cells( 0 )
#ifdef CXX_0X
           ,
           _min{ 0 },
           _max{ 0 },
           _sides{ 0 },
           _delta{ 0 }
#endif
      {
#ifndef CXX_0X
         for( unsigned ii = 0; ii < D; ++ii )
         {
            _min[ii] = 0;
            _max[ii] = 0;
            _sides[ii] = 0;
            _delta[ii] = 0;
         }
#endif
      }

      CUDA_DEV_HOST
      grid( boost::array<real_type,D> const& min,
            boost::array<real_type,D> const& max,
            boost::array<uint16_t,D> const& sides )
#ifdef CXX_0X
           ,
           _min{ 0 },
           _max{ 0 },
           _sides{ 0 },
           _delta{ 0 }
#endif
      {
#ifndef CXX_0X
         for( unsigned ii = 0; ii < D; ++ii )
         {
            _min[ii] = 0;
            _max[ii] = 0;
            _sides[ii] = 0;
            _delta[ii] = 0;
         }
#endif
         set_dimensions( min, max );
         set_sides( sides );
      }

      CUDA_DEV_HOST
      grid( boost::array<real_type,D> const& min,
            boost::array<real_type,D> const& max,
            real_type delta )
#ifdef CXX_0X
           ,
           _min{ 0 },
           _max{ 0 },
           _sides{ 0 },
           _delta{ 0 }
#endif
      {
#ifndef CXX_0X
         for( unsigned ii = 0; ii < D; ++ii )
         {
            _min[ii] = 0;
            _max[ii] = 0;
            _sides[ii] = 0;
            _delta[ii] = 0;
         }
#endif
         set_dimensions( min, max );
         set_delta( delta );
      }

      template< class Other,
                typename boost::enable_if<boost::is_base_of<grid,Other>,void>::type >
      CUDA_DEV_HOST
      grid( Other const& src )
         : _min( src.min ),
           _max( src.max ),
           _sides( src.sides ),
           _delta( src.delta ),
           _n_cells( src.n_cells )
      {
      }

      CUDA_DEV_HOST
      void
      set_dimensions( boost::array<real_type,D> const& min,
                      boost::array<real_type,D> const& max )
      {
#ifndef NDEBUG
         for( unsigned ii = 0; ii < D; ++ii )
            ASSERT( max[ii] - min[ii] > 0.0, "Grid size <= 0 in dimension: ", ii );
#endif
         _min = min;
         _max = max;
         update();
      }

#ifdef CXX_0X
      constexpr
#endif
      CUDA_DEV_HOST
      unsigned
      n_dims() const
      {
         return n_dims_type;
      }

      CUDA_DEV_HOST
      void
      set_sides( boost::array<uint16_t,D> const& sides )
      {
         _sides = sides;
         update();
      }

      CUDA_DEV_HOST
      boost::array<real_type,D> const&
      min() const
      {
         return _min;
      }

      CUDA_DEV_HOST
      boost::array<real_type,D> const&
      max() const
      {
         return _max;
      }

      CUDA_DEV_HOST
      void
      set_delta( real_type delta )
      {
         ASSERT( delta > 0.0, "Invalid grid delta." );
         std::fill( _delta.begin(), _delta.end(), delta );
         update();
      }

      CUDA_DEV_HOST
      boost::array<real_type,D> const&
      delta() const
      {
         return _delta;
      }

      CUDA_DEV_HOST
      boost::array<uint16_t,D> const&
      sides() const
      {
         return _sides;
      }

      CUDA_DEV_HOST
      uint32_t
      n_cells() const
      {
         return _n_cells;
      }

      CUDA_DEV_HOST
      neighbor_iterator
      begin_neighbors( uint32_t idx,
		       real_type r_cut ) const
      {
	 return neighbor_iterator( (Final*)this, r_cut, idx );
      }

      CUDA_DEV_HOST
      neighbor_iterator
      end_neighbors( uint32_t idx,
		     real_type r_cut ) const
      {
	 return neighbor_iterator();
      }

      CUDA_DEV_HOST
      void
      update()
      {
         // If we have geometry and delta calculate sides.
         if( _max[0] > _min[0] && _delta[0] > 0.0 )
         {
            for( unsigned ii = 0; ii < D; ++ii )
               _sides[ii] = (_max[ii] - _min[ii])/_delta[ii] + 1;
         }

         // Update delta and number of cells.
         if( _max[0] > _min[0] && _sides[0] > 0 )
         {
            _n_cells = 1;
            for( unsigned ii = 0; ii < D; ++ii )
            {
               _delta[ii] = (_max[ii] - _min[ii])/(real_type)_sides[ii];
               _n_cells *= _sides[ii];
            }
         }
      }

   protected:

      boost::array<real_type,D> _min, _max;
      boost::array<uint16_t,D> _sides;
      boost::array<real_type,D> _delta;
      uint32_t _n_cells;
   };

   namespace grids {

      ///
      /// Uniform grid.
      ///
      template< unsigned D >
      class uniform
         : public grid<D,uniform<D> >
      {
      public:

         typedef grid<D,uniform<D> > super_type;

      public:

         CUDA_DEV_HOST
         uniform()
            : super_type()
         {
         }

         CUDA_DEV_HOST
         uniform( boost::array<real_type,D> const& min,
                  boost::array<real_type,D> const& max,
                  boost::array<uint16_t,D> const& sides )
            : super_type( min, max, sides )
         {
         }

         CUDA_DEV_HOST
         uniform( boost::array<real_type,D> const& min,
                  boost::array<real_type,D> const& max,
                  real_type delta )
            : super_type( min, max, delta )
         {
         }

         CUDA_DEV_HOST
         uint32_t
         project( boost::array<uint16_t,D> const& crds ) const
         {
            return hpc::project<uint16_t,uint32_t,D>( crds, this->_sides );
         }

         CUDA_DEV_HOST
         boost::array<uint16_t,D>
         lift( uint32_t idx ) const
         {
            return hpc::lift<uint32_t,uint16_t,D>( idx, this->_sides );
         }
      };

      ///
      /// Uniform grid with Morton ordering.
      ///
      template< unsigned D >
      class morton
         : public grid<D,morton<D> >
      {

      public:

         typedef grid<D,morton<D> > super_type;

         using super_type::_sides;
         using super_type::update;

      public:

         morton()
            : super_type()
         {
         }

         morton( boost::array<real_type,D> const& min,
                 boost::array<real_type,D> const& max,
                 boost::array<uint16_t,D> const& sides )
            : super_type( min, max, sides )
         {
         }

         morton( boost::array<real_type,D> const& min,
                 boost::array<real_type,D> const& max,
                 real_type delta )
            : super_type( min, max, delta )
         {
         }

         void
         set_sides( boost::array<uint16_t,D> const& sides )
         {
            // Must be power of 2 in size in all dimensions in
            // order to not have gaps in the numbering.
            for( unsigned ii = 0; ii < D; ++ii )
            {
               EXCEPT( hpc::nearest_po2( _sides[ii] ) == this->_sides[ii],
                       "When using a morton grid all sides must be power-of-2." );
            }

            _sides = sides;
            update();
         }

         uint32_t
         project( boost::array<uint16_t,D> const& crds ) const
         {
#ifndef NDEBUG
            for( unsigned ii = 0; ii < D; ++ii )
               ASSERT( crds[ii] >= 0 && crds[ii] < this->_sides[ii], "Invalid grid coordinate." );
#endif
            return hpc::morton_array( crds );
         }

         boost::array<uint16_t,D>
         lift( uint32_t idx ) const
         {
            ASSERT( idx < this->_n_cells, "Invalid grid cell index." );
            return hpc::unmorton<D>( idx );
         }
      };

   }
}

#endif
