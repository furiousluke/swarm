#ifndef swarm_types_hh
#define swarm_types_hh

namespace swarm {

   typedef double real_type;

#ifdef __CUDACC__

   typedef double3 real3_type;
   #define make_real3 make_double3

   typedef double3 pos3_type;
   #define make_pos3 make_double3

#endif

}

#endif
