#ifndef swarm_interaction_hh
#define swarm_interaction_hh

#ifdef __CUDACC__
#include <thrust/tuple.h>
#endif
#include <libhpc/system/cuda.hh>
#include "types.hh"

namespace swarm {
   namespace interactions {

      template< class Data >
      class density
      {
      public:

	 typedef Data data_type;
	 static const unsigned n_dims = data_type::n_dims;

      public:

         CUDA_DEV_HOST
         void
	 operator()( data_type& part_i,
		     data_type& part_j,
		     real_type kern,
		     real_type mass ) const
	 {
#ifndef __CUDACC__
	    std::get<n_dims>( part_i.data ) += mass*kern;
#else
            thrust::get<n_dims>( part_i.data ) += mass*kern;
#endif
	 }
      };

   }
}

#endif
