#ifndef swarm_cuda_summation_hh
#define swarm_cuda_summation_hh

#include <boost/utility/enable_if.hpp>
#include <libhpc/algorithm/uniform.hh>
#include "swarm/types.hh"

namespace swarm {
   namespace cuda {

      template< class Interaction >
      class summation_base
      {
      public:

	 typedef Interaction interaction_type;

      public:

      	 summation_base( interaction_type  inter,
      			 swarm::real_type* x,
      			 swarm::real_type* h,
      			 swarm::real_type* rho,
      			 swarm::real_type* omega,
      			 uint32_t const*   displs,
      			 uint32_t const*   cells,
      			 uint16_t          grid_i,
      			 uint16_t          grid_j,
      			 uint16_t          grid_k,
      			 swarm::real_type  delta_i,
      			 swarm::real_type  delta_j,
      			 swarm::real_type  delta_k,
      			 swarm::real_type  eta )
      	    : _inter( inter ),
      	      _x( x ),
      	      _h( h ),
      	      _rho( rho ),
      	      _omega( omega ),
      	      _displs( displs ),
      	      _cells( cells ),
      	      _eta( eta )
      	 {
      	    _grid_sides = hpc::make_array( grid_i, grid_j, grid_k );
      	    _grid_delta = hpc::make_array( delta_i, delta_j, delta_k );
      	 }

	 template< class SubInteraction >
      	 __device__ __host__
	 typename boost::enable_if<typename SubInteraction::periodic_type>::type
      	 interaction( swarm::pos3_type const& x_i,
      		      swarm::real_type        h_i,
      		      uint                    p_j,
      		      swarm::real_type        r_cut )
      	 {
      	    swarm::pos3_type x_j      = make_pos3( _x[3*p_j + 0] + _poffs.x,
      						   _x[3*p_j + 1] + _poffs.y,
      						   _x[3*p_j + 2] + _poffs.z );
      	    swarm::pos3_type x_ij     = make_pos3( x_j.x - x_i.x, x_j.y - x_i.y, x_j.z - x_i.z );
      	    swarm::real_type r_ij_sq  = x_ij.x*x_ij.x + x_ij.y*x_ij.y + x_ij.z*x_ij.z;
      	    swarm::real_type r_cut_sq = r_cut*r_cut;
      	    if( r_ij_sq < r_cut_sq )
      	       _inter( h_i, p_j, x_ij, r_ij_sq, _rho, _h, _omega );
      	 }

	 template< class SubInteraction >
      	 __device__ __host__
      	 typename boost::disable_if<typename SubInteraction::periodic_type>::type
      	 interaction( swarm::pos3_type const& x_i,
      		      swarm::real_type        h_i,
      		      uint                    p_j,
      		      swarm::real_type        r_cut )
      	 {
      	    swarm::pos3_type x_j      = make_pos3( _x[3*p_j + 0], _x[3*p_j + 1], _x[3*p_j + 2] );
      	    swarm::pos3_type x_ij     = make_pos3( x_j.x - x_i.x, x_j.y - x_i.y, x_j.z - x_i.z );
      	    swarm::real_type r_ij_sq  = x_ij.x*x_ij.x + x_ij.y*x_ij.y + x_ij.z*x_ij.z;
      	    swarm::real_type r_cut_sq = r_cut*r_cut;
      	    if( r_ij_sq < r_cut_sq )
      	       _inter( h_i, p_j, x_ij, r_ij_sq, _rho, _h, _omega );
      	 }

	 template< class SubInteraction >
      	 __device__ __host__
	 typename boost::enable_if<typename SubInteraction::exclusive_type>::type
      	 process_neighbor_cell( uint                    p_i,
				swarm::pos3_type const& x_i,
				swarm::real_type        h_i,
				uint                    nbr,
				swarm::real_type        r_cut )
      	 {
      	    uint part_begin = _displs[nbr];
      	    uint part_end   = _displs[nbr + 1];
      	    for( uint p_j = part_begin; p_j < part_end; ++p_j )
	    {
	       if( p_j != p_i )
		  interaction<interaction_type>( x_i, h_i, p_j, r_cut );
	    }
      	 }

	 template< class SubInteraction >
      	 __device__ __host__
	 typename boost::disable_if<typename SubInteraction::exclusive_type>::type
      	 process_neighbor_cell( uint                    p_i,
				swarm::pos3_type const& x_i,
				swarm::real_type        h_i,
				uint                    nbr,
				swarm::real_type        r_cut )
      	 {
      	    uint part_begin = _displs[nbr];
      	    uint part_end   = _displs[nbr + 1];
      	    for( uint p_j = part_begin; p_j < part_end; ++p_j )
	       interaction<interaction_type>( x_i, h_i, p_j, r_cut );
      	 }

	 template< class SubInteraction >
      	 __device__ __host__
	 typename boost::enable_if<typename SubInteraction::periodic_type>::type
      	 iterate_neighbors( uint p_i,
			    swarm::pos3_type const& x_i,
			    swarm::real_type h_i,
			    swarm::real_type r_cut,
			    hpc::array<ushort,3> const& org,
			    ushort3 const& step )
      	 {
      	    int3 low = make_int3( (int)org[0] - (int)step.x,
      				  (int)org[1] - (int)step.y,
      				  (int)org[2] - (int)step.z );
      	    int3 upp = make_int3( (int)org[0] + (int)step.x + 1,
      				  (int)org[1] + (int)step.y + 1,
      				  (int)org[2] + (int)step.z + 1 );

      	    // Iterate over neighboring grid cells, including periodic ones.
      	    hpc::array<int,3> nbr_crd;
      	    hpc::array<ushort,3> real_nbr_crd;
      	    for( nbr_crd[2] = low.z; nbr_crd[2] < upp.z; ++nbr_crd[2] )
      	    {
      	       // k-periodicity.
      	       if( nbr_crd[2] < 0 )
      	       {
      		  real_nbr_crd[2] = (ushort)(nbr_crd[2] + (int)_grid_sides[2]);
      		  _poffs.z = -((swarm::real_type)_grid_sides[2])*_grid_delta[2];
      	       }
      	       else if( nbr_crd[2] >= (int)_grid_sides[2] )
      	       {
      		  real_nbr_crd[2] = (ushort)(nbr_crd[2] - (int)_grid_sides[2]);
      		  _poffs.z = ((swarm::real_type)_grid_sides[2])*_grid_delta[2];
      	       }
      	       else
      	       {
      		  real_nbr_crd[2] = (ushort)nbr_crd[2];
      		  _poffs.z = 0.0;
      	       }

      	       for( nbr_crd[1] = low.y; nbr_crd[1] < upp.y; ++nbr_crd[1] )
      	       {
      		  // j-periodicity
      		  if( nbr_crd[1] < 0 )
      		  {
      		     real_nbr_crd[1] = (ushort)(nbr_crd[1] + (int)_grid_sides[1]);
      		     _poffs.y = -((swarm::real_type)_grid_sides[1])*_grid_delta[1];
      		  }
      		  else if( nbr_crd[1] >= (int)_grid_sides[1] )
      		  {
      		     real_nbr_crd[1] = (ushort)(nbr_crd[1] - (int)_grid_sides[1]);
      		     _poffs.y = ((swarm::real_type)_grid_sides[1])*_grid_delta[1];
      		  }
      		  else
      		  {
      		     real_nbr_crd[1] = (ushort)nbr_crd[1];
      		     _poffs.y = 0.0;
      		  }

      		  for( nbr_crd[0] = low.x; nbr_crd[0] < upp.x; ++nbr_crd[0] )
      		  {
      		     // i-periodicity.
      		     if( nbr_crd[0] < 0 )
      		     {
      			real_nbr_crd[0] = (ushort)(nbr_crd[0] + (int)_grid_sides[0]);
      			_poffs.x = -((swarm::real_type)_grid_sides[0])*_grid_delta[0];
      		     }
      		     else if( nbr_crd[0] >= (int)_grid_sides[0] )
      		     {
      			real_nbr_crd[0] = (ushort)(nbr_crd[0] - (int)_grid_sides[0]);
      			_poffs.x = ((swarm::real_type)_grid_sides[0])*_grid_delta[0];
      		     }
      		     else
      		     {
      			real_nbr_crd[0] = (ushort)nbr_crd[0];
      			_poffs.x = 0.0;
      		     }

      		     // Project to the flattened grid index and process.
      		     uint idx = hpc::project<ushort,uint,3,ushort>( real_nbr_crd, _grid_sides );
      		     process_neighbor_cell<interaction_type>( p_i, x_i, h_i, idx, r_cut );
      		  }
      	       }
      	    }
      	 }

	 template< class SubInteraction >
      	 __device__ __host__
      	 typename boost::disable_if<typename SubInteraction::periodic_type>::type
      	 iterate_neighbors( uint p_i,
			    swarm::pos3_type const& x_i,
			    swarm::real_type h_i,
			    swarm::real_type r_cut,
			    hpc::array<ushort,3> const& org,
			    ushort3 const& step )
      	 {
      	    ushort3 low = make_ushort3( (org[0] > step.x) ? (org[0] - step.x) : 0,
      					(org[1] > step.y) ? (org[1] - step.y) : 0,
      					(org[2] > step.z) ? (org[2] - step.z) : 0 );
      	    ushort3 upp = make_ushort3( (_grid_sides[0] - org[0] > step.x) ? (org[0] + step.x + 1) : _grid_sides[0],
      					(_grid_sides[1] - org[1] > step.y) ? (org[1] + step.y + 1) : _grid_sides[1],
      					(_grid_sides[2] - org[2] > step.z) ? (org[2] + step.z + 1) : _grid_sides[2] );

      	    // Iterate over neighboring grid cells.
      	    hpc::array<ushort,3> nbr_crd;
      	    for( nbr_crd[2] = low.z; nbr_crd[2] < upp.z; ++nbr_crd[2] )
      	    {
      	       for( nbr_crd[1] = low.y; nbr_crd[1] < upp.y; ++nbr_crd[1] )
      	       {
      		  for( nbr_crd[0] = low.x; nbr_crd[0] < upp.x; ++nbr_crd[0] )
      		  {
      		     // Project to the flattened grid index and process.
      		     uint idx = hpc::project<ushort,uint,3,ushort>( nbr_crd, _grid_sides );
      		     process_neighbor_cell<interaction_type>( p_i, x_i, h_i, idx, r_cut );
      		  }
      	       }
      	    }
      	 }

      protected:

      	 swarm::real_type*              _x;
      	 swarm::real_type*              _h;
      	 swarm::real_type*              _rho;
      	 swarm::real_type*              _omega;
      	 uint32_t const*                _displs;
      	 uint32_t const*                _cells;
      	 hpc::array<ushort,3>           _grid_sides;
      	 hpc::array<swarm::real_type,3> _grid_delta;
      	 swarm::real_type               _eta;
      	 interaction_type               _inter;
      	 swarm::real3_type              _poffs;
      };

      template< class Interaction >
      class density_summation
	 : public summation_base<Interaction>
      {
      public:

	 typedef summation_base<Interaction> super_type;
	 typedef Interaction                 interaction_type;
	 typedef typename interaction_type::kernel_type kernel_type;

	 using super_type::_inter;
	 using super_type::_x;
	 using super_type::_h;
	 using super_type::_rho;
	 using super_type::_omega;
	 using super_type::_cells;
	 using super_type::_grid_sides;
	 using super_type::_grid_delta;
	 using super_type::_eta;

      public:

	 density_summation( interaction_type inter,
			    swarm::real_type* x,
			    swarm::real_type* h,
			    swarm::real_type* rho,
			    swarm::real_type* omega,
			    uint32_t const*   displs,
			    uint32_t const*   cells,
			    uint16_t          grid_i,
			    uint16_t          grid_j,
			    uint16_t          grid_k,
			    swarm::real_type  delta_i,
			    swarm::real_type  delta_j,
			    swarm::real_type  delta_k,
			    swarm::real_type  eta )
	    : super_type( inter,
			  x, h, rho, omega,
			  displs, cells,
			  grid_i, grid_j, grid_k,
			  delta_i, delta_j, delta_k,
			  eta )
	 {
	 }

	 __device__ __host__
	 void
	 operator()( uint p_i )
	 {
	    // Get the cell index.
	    uint cell = _cells[p_i];

	    // Get the grid origin for this cell.
	    hpc::array<ushort,3> org = hpc::lift<uint,ushort,3,ushort>( cell, _grid_sides );

	    // Cache some things for performance.
	    swarm::pos3_type x_i = make_pos3( _x[3*p_i + 0], _x[3*p_i + 1], _x[3*p_i + 2] );
	    swarm::real_type h_i = _h[p_i];

	    // Run a Newton-Raphson solve.
	    swarm::real_type delta;
	    uint its = 0;
	    do
	    {
	       // Clear particle data before summation.
	       _inter.reset();

	       // Calculate grid metrics.
	       swarm::real_type r_cut = h_i*(swarm::real_type)kernel_type::radius;
	       ushort3 step = make_ushort3( (ushort)(r_cut/_grid_delta[0]) + 1,
					    (ushort)(r_cut/_grid_delta[1]) + 1,
					    (ushort)(r_cut/_grid_delta[2]) + 1 );

	       // Call out to the neighbor iteration.
	       this->template iterate_neighbors<interaction_type>( p_i, x_i, h_i, r_cut, org, step );

	       // Claculate the Newton-Raphson values and check if we're done.
	       swarm::real_type rho_i = _inter.rho_i();
	       swarm::real_type dwdh = _inter.dwdh_i();
	       swarm::real_type new_h = _eta*pow( 1.0/rho_i, 1.0/3.0 );
	       swarm::real_type f     = h_i - new_h;
	       swarm::real_type dfdh = (_eta*dwdh*pow( 1.0/rho_i, 1.0/3.0 - 1.0 ))/(3.0*rho_i*rho_i) + 1.0;
	       delta = f/dfdh;
	       h_i -= delta;
	    }
	    while( fabs( delta ) > 1e-5 && ++its < 60 );

	    // Update h and other data values.
	    _rho[p_i]   = _inter.rho_i();
	    _h[p_i]     = h_i;
	    _omega[p_i] = 1.0 - (h_i/(3.0*_inter.rho_i()))*_inter.dwdh_i();
	 }
      };

      template< class Interaction >
      class summation
	 : public summation_base<Interaction>
      {
      public:

	 typedef summation_base<Interaction> super_type;
	 typedef Interaction                 interaction_type;
	 typedef typename interaction_type::kernel_type kernel_type;

	 using super_type::_inter;
	 using super_type::_x;
	 using super_type::_h;
	 using super_type::_rho;
	 using super_type::_omega;
	 using super_type::_cells;
	 using super_type::_grid_sides;
	 using super_type::_grid_delta;

      public:

	 summation( interaction_type  inter,
		    swarm::real_type* x,
		    swarm::real_type* h,
		    swarm::real_type* rho,
		    swarm::real_type* omega,
		    uint32_t const*   displs,
		    uint32_t const*   cells,
		    uint16_t          grid_i,
		    uint16_t          grid_j,
		    uint16_t          grid_k,
		    swarm::real_type  delta_i,
		    swarm::real_type  delta_j,
		    swarm::real_type  delta_k,
		    swarm::real_type  eta )
	    : super_type( inter,
			  x, h, rho, omega,
			  displs, cells,
			  grid_i, grid_j, grid_k,
			  delta_i, delta_j, delta_k,
			  eta )
	 {
	 }

	 __device__ __host__
	 void
	 operator()( uint p_i )
	 {
	    // Get the cell index.
	    uint cell = _cells[p_i];

	    // Get the grid origin for this cell.
	    hpc::array<ushort,3> org = hpc::lift<uint,ushort,3,ushort>( cell, _grid_sides );

	    // Cache some things for performance.
	    swarm::pos3_type x_i = make_pos3( _x[3*p_i + 0], _x[3*p_i + 1], _x[3*p_i + 2] );
	    swarm::real_type h_i = _h[p_i];

	    // Have the interaction cache i information.
	    _inter.cache_i( p_i, _rho, _h, _omega );

	    // Do any initial setup.
	    _inter.start( p_i, _x );

	    // Clear particle data before summation.
	    _inter.reset();

	    // Calculate grid metrics.
	    swarm::real_type r_cut = h_i*(swarm::real_type)kernel_type::radius;
	    ushort3 step = make_ushort3( (ushort)(r_cut/_grid_delta[0]) + 1,
					 (ushort)(r_cut/_grid_delta[1]) + 1,
					 (ushort)(r_cut/_grid_delta[2]) + 1 );

	    // Call out to the neighbor iteration.
	    this->template iterate_neighbors<interaction_type>( p_i, x_i, h_i, r_cut, org, step );

	    // Finish interactions.
	    _inter.finish( p_i );
	 }
      };

   }
}

#endif
