#ifndef swarm_cuda_interaction_hh
#define swarm_cuda_interaction_hh

#include <boost/type_traits/integral_constant.hpp>
#include "swarm/types.hh"

namespace swarm {
   namespace cuda {

      template< class Kernel,
		bool Periodic = true >
      class density
      {
      public:

	 typedef Kernel                                  kernel_type;
	 typedef boost::integral_constant<bool,Periodic> periodic_type;
	 typedef boost::false_type                       exclusive_type;

      public:

	 __device__ __host__
	 void
	 reset()
	 {
	    _rho_i = 0.0;
	    _dwdh_i = 0.0;
	 }

	 __device__ __host__
	 void
	 operator()( swarm::real_type         h_i,
		     uint                     p_j,
		     swarm::real3_type const& x_ij,
		     swarm::real_type         r_ij_sq,
		     swarm::real_type const*  rho,
		     swarm::real_type const*  h,
		     swarm::real_type const*  omega )
	 {
	    hpc::array<swarm::real_type,2> val = _kern.deriv_h( r_ij_sq, h_i );
	    _rho_i += val[0];
	    _dwdh_i += val[1];
	 }

	 __device__ __host__
	 swarm::real_type
	 rho_i() const
	 {
	    return _rho_i;
	 }

	 __device__ __host__
	 swarm::real_type
	 dwdh_i() const
	 {
	    return _rho_i;
	 }

      protected:

	 kernel_type _kern;
	 swarm::real_type _rho_i;
	 swarm::real_type _dwdh_i;
      };

      template< class Kernel,
		class EquationOfState,
		bool Periodic = true >
      class pressure_force
      {
      public:

	 typedef Kernel                                  kernel_type;
	 typedef EquationOfState                         eos_type;
	 typedef boost::integral_constant<bool,Periodic> periodic_type;
	 typedef boost::true_type                        exclusive_type;

      public:

	 pressure_force( swarm::real_type* v,
			 bool first )
	    : _v( v ),
	      _dt( 0.2 ),
	      _first( first )
	 {
	 }

	 __device__ __host__
	 void
	 cache_i( uint p_i,
		  swarm::real_type const* rho,
		  swarm::real_type const* h,
		  swarm::real_type const* omega )
	 {
	    _rho_i = rho[p_i];
	    _omega_i = omega[p_i];
	 }

	 __device__ __host__
	 void
	 start( uint p_i,
		swarm::real_type* x )
	 {
	    // Advance positions by the half-step velocity. Only do this
	    // if this is not the first step.
	    if( !_first )
	    {
	       x[3*p_i + 0] += _v[3*p_i + 0]*_dt;
	       x[3*p_i + 1] += _v[3*p_i + 1]*_dt;
	       x[3*p_i + 2] += _v[3*p_i + 2]*_dt;
	    }
	 }

	 __device__ __host__
	 void
	 reset()
	 {
	    _a_i = make_real3( 0.0, 0.0, 0.0 );
	 }

	 __device__ __host__
	 void
	 finish( uint p_i )
	 {
	    // Advance velocity to the next half-step, or advance from
	    // the first whole step to a half step.
	    if( !_first )
	    {
	       _v[3*p_i + 0] += _a_i.x*_dt;
	       _v[3*p_i + 1] += _a_i.y*_dt;
	       _v[3*p_i + 2] += _a_i.z*_dt;
	    }
	    else
	    {
	       _v[3*p_i + 0] += 0.5*_a_i.x*_dt;
	       _v[3*p_i + 1] += 0.5*_a_i.y*_dt;
	       _v[3*p_i + 2] += 0.5*_a_i.z*_dt;
	    }
	 }

	 __device__ __host__
	 void
	 operator()( swarm::real_type         h_i,
		     uint                     p_j,
		     swarm::real3_type const& x_ij,
		     swarm::real_type         r_ij_sq,
		     swarm::real_type const*  rho,
		     swarm::real_type const*  h,
		     swarm::real_type const*  omega )
	 {
	    swarm::real_type h_j      = h[p_j];
	    swarm::real_type rho_j    = rho[p_j];
	    swarm::real_type omega_j  = omega[p_j];
	    swarm::real_type r_ij_inv = 1.0/swarm::sqrt( r_ij_sq );
	    swarm::real_type dWdr_i   = _kern.deriv( r_ij_sq, h_i );
	    swarm::real_type dWdr_j   = _kern.deriv( r_ij_sq, h_j );
	    swarm::real_type pre_i    = _eos( _rho_i );
	    swarm::real_type pre_j    = _eos( rho_j );
	    _a_i.x += -((pre_i/(_omega_i*_rho_i*_rho_i))*dWdr_i + (pre_j/(omega_j*rho_j*rho_j))*dWdr_j)*x_ij.x*r_ij_inv;
	    _a_i.y += -((pre_i/(_omega_i*_rho_i*_rho_i))*dWdr_i + (pre_j/(omega_j*rho_j*rho_j))*dWdr_j)*x_ij.y*r_ij_inv;
	    _a_i.z += -((pre_i/(_omega_i*_rho_i*_rho_i))*dWdr_i + (pre_j/(omega_j*rho_j*rho_j))*dWdr_j)*x_ij.z*r_ij_inv;
	 }

      protected:

	 kernel_type _kern;
	 eos_type _eos;
	 swarm::real3_type _a_i;
	 swarm::real_type  _rho_i;
	 swarm::real_type  _omega_i;
	 swarm::real_type* _v;
	 swarm::real_type _dt;
	 bool _first;
      };

   }
}

#endif
