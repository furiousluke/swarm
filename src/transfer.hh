#ifndef swarm_transfer_hh
#define swarm_transfer_hh

#include <thrust/device_vector.h>

namespace swarm {

   template< class DataVec,
	     class DevVec >
   void
   htd_x_h( DataVec const& data,
	    DevVec& dev_x,
	    DevVec& dev_h )
   {
      typedef typename DataVec::size_type size_type;

      {
	 thrust::host_vector<swarm::pos_type> buf( 3*data.size() );
	 for( size_type ii = 0; ii < data.size(); ++ii )
	 {
	    size_type offs = 3*ii;
	    buf[offs + 0] = thrust::get<0>( data[ii].data );
	    buf[offs + 1] = thrust::get<1>( data[ii].data );
	    buf[offs + 2] = thrust::get<2>( data[ii].data );
	 }
	 dev_x = buf;
      }

      {
	 thrust::host_vector<swarm::h_type> buf( data.size() );
	 for( size_type ii = 0; ii < data.size(); ++ii )
	    buf[ii] = thrust::get<4>( data[ii].data );
	 dev_h = buf;
      }
   }

   template< class DataVec,
	     class DevVec >
   void
   dth_x_v_h_rho( DataVec& data,
		  thrust::host_vector<swarm::real_type>& host_v,
		  DevVec const& dev_x,
		  DevVec const& dev_v,
		  DevVec const& dev_h,
		  DevVec const& dev_rho )
   {
      typedef typename DataVec::size_type size_type;

      {
	 thrust::host_vector<swarm::pos_type> buf( dev_x );
	 for( size_type ii = 0; ii < data.size(); ++ii )
	 {
	    size_type offs = 3*ii;
	    thrust::get<0>( data[ii].data ) = buf[offs + 0];
	    thrust::get<1>( data[ii].data ) = buf[offs + 1];
	    thrust::get<2>( data[ii].data ) = buf[offs + 2];
	 }
      }

      {
	 thrust::host_vector<swarm::h_type> buf( dev_h );
	 for( size_type ii = 0; ii < data.size(); ++ii )
	    thrust::get<4>( data[ii].data ) = buf[ii];
      }

      {
	 thrust::host_vector<swarm::density_type> buf( dev_rho );
	 for( size_type ii = 0; ii < data.size(); ++ii )
	    thrust::get<3>( data[ii].data ) = buf[ii];
      }

      host_v = dev_v;
   }

}

#endif
