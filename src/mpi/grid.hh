#ifndef swarm_mpi_grid_hh
#define swarm_mpi_grid_hh

#include <libhpc/mpi/mpi.hh>
#include <libhpc/algorithm/uniform.hh>

namespace swarm {
   namespace mpi {
      namespace grids {

         template< class Serial >
         class block
            : public Serial
         {
         public:

            typedef Serial serial_type;
            static constexpr unsigned n_dims = serial_type::n_dims;
	    static constexpr bool is_parallel = true;

         public:

            template< class... Args >
            block( Args&&... args )
               : Serial( std::forward<Args>( args )... ),
		 _comm( &hpc::mpi::comm::world ),
		 _shad_depth( 2 )
            {
	       update();
            }

            uint64_t
            to_global( uint32_t lidx )
            {
               auto crd = this->lift( lidx );
               std::array<uint32_t,n_dims> g_crd;
               std::transform( crd.begin(), crd.end(), _g_offs.begin(), g_crd.begin(), std::plus<uint32_t>() );
               return hpc::project<uint32_t,uint64_t,n_dims>( g_crd, _g_sides );
            }

            uint32_t
            to_local( uint64_t gidx )
            {
               auto crd = hpc::lift<uint64_t,uint32_t,n_dims>( gidx, _g_sides );
#ifndef NDEBUG
               for( unsigned ii = 0; ii < n_dims; ++ii )
               {
                  ASSERT( crd[ii] >= _g_offs[ii] && crd[ii] < _g_offs[ii] + _g_sides[ii],
                          "Global coordinate is not local to rank." );
               }
#endif
               std::array<uint16_t,n_dims> l_crd;
               std::transform( crd.begin(), crd.end(), _g_offs.begin(), l_crd.begin(), std::minus<uint32_t>() );
               return this->project( l_crd );
            }

	    bool
	    is_local( uint32_t cell ) const
	    {
	       auto crd = this->lift( cell );
	       for( unsigned ii = 0; ii < n_dims; ++ii )
	       {
		  if( crd[ii] < _shad_zones[ii][0] || crd[ii] >= _shad_zones[ii][1] )
		     return false;
	       }
	       return true;
	    }

	    std::array<uint16_t,n_dims> const&
	    local_sides() const
	    {
	       return _l_sides;
	    }

	    std::array<uint32_t,n_dims> const&
	    global_sides() const
	    {
	       return _g_sides;
	    }

	    std::array<uint32_t,n_dims> const&
	    global_offset() const
	    {
	       return _g_offs;
	    }

	    std::array<double,n_dims> const&
	    local_min() const
	    {
	       return _l_min;
	    }

	    std::array<double,n_dims> const&
	    local_max() const
	    {
	       return _l_max;
	    }

	    unsigned
	    n_local_cells() const
	    {
	       return _n_l_cells;
	    }

	    unsigned
	    n_shadow_cells() const
	    {
	       return _shad_cells.size();
	    }

	    std::vector<uint64_t> const&
	    shadow_displs() const
	    {
	       return _shad_displs;
	    }

	    std::vector<uint32_t> const&
	    shadow_cells() const
	    {
	       return _shad_cells;
	    }

	    hpc::view<std::vector<uint32_t>>::type const
	    shadow_cells( unsigned nbr ) const
	    {
	       auto offs = _shad_displs[nbr];
	       return hpc::view<std::vector<uint32_t>>::type( _shad_cells, _shad_displs[nbr + 1] - offs, offs );
	    }

	    unsigned
	    n_halo_cells() const
	    {
	       return _halo_cells.size();
	    }

	    std::vector<uint64_t> const&
	    halo_displs() const
	    {
	       return _halo_displs;
	    }

	    std::vector<uint32_t> const&
	    halo_cells() const
	    {
	       return _halo_cells;
	    }

	    hpc::view<std::vector<uint32_t>>::type const
	    halo_cells( unsigned nbr ) const
	    {
	       auto offs = _halo_displs[nbr];
	       return hpc::view<std::vector<uint32_t>>::type( _halo_cells, _halo_displs[nbr + 1] - offs, offs );
	    }

	    hpc::mpi::comm const*
	    comm() const
	    {
	       return _comm;
	    }

	    hpc::mpi::vct const&
	    vct() const
	    {
	       return _vct;
	    }

            void
            update()
            {
               // Only update if both delta and sizes set. Calculate the global
               // details here.
               if( this->_max[0] > this->_min[0] && this->_delta[0] > 0.0 )
               {
                  LOGBLOCKD( "Block decomposition." );

		  // Make sure global values kept.
		  std::copy( this->_min.begin(), this->_min.end(), _g_min.begin() );
		  std::copy( this->_max.begin(), this->_max.end(), _g_max.begin() );

                  _n_g_cells = 1;
                  for( unsigned ii = 0; ii < n_dims; ++ii )
                  {
                     _g_sides[ii] = (_g_max[ii] - _g_min[ii])/this->_delta[ii] + 1;
                     this->_delta[ii] = (_g_max[ii] - _g_min[ii])/static_cast<double>( _g_sides[ii] );
                     _n_g_cells *= _g_sides[ii];
                  }
                  LOGDLN( "Global grid size: ", _g_sides );
		  LOGDLN( "Delta: ", this->_delta );

                  // Calculate local details and decomposition. For now we just require
                  // a perfect fit.
                  unsigned rank = _comm->rank();
                  unsigned n_ranks = _comm->size();
                  unsigned proc_sides = ::pow( static_cast<double>( n_ranks ), 1.0/static_cast<double>( n_dims ) );
                  EXCEPT( static_cast<unsigned>( ::pow( proc_sides, static_cast<double>( n_dims ) ) ) == n_ranks,
                          "Invalid number of processes." );
                  LOGDLN( "Processor sides: ", proc_sides );

                  // Calculate my global local region.
                  _proc_grid.resize( n_dims, proc_sides );
                  std::array<unsigned,n_dims> proc_crd;
                  _proc_grid.lift( rank, proc_crd.begin() );
		  _n_l_cells = 1;
                  for( unsigned ii = 0; ii < n_dims; ++ii )
                  {
                     _g_offs[ii] = proc_crd[ii]*static_cast<uint64_t>( _g_sides[ii] )/proc_sides;
                     this->_sides[ii] = (proc_crd[ii] + 1)*static_cast<uint64_t>( _g_sides[ii] )/proc_sides - _g_offs[ii];
		     _n_l_cells *= this->_sides[ii];
                  }
		  _l_sides = this->_sides;
		  _l_offs = _g_offs;
                  LOGDLN( "Local offset: ", _l_offs );
                  LOGDLN( "Local size: ", this->_sides );

                  // Calculate halo cells, expanding the local region to fit.
                  for( unsigned ii = 0; ii < n_dims; ++ii )
                  {
                     if( _g_offs[ii] > _shad_depth )
                     {
                        _g_offs[ii] -= _shad_depth;
                        this->_sides[ii] += _shad_depth;
                        _shad_zones[ii][0] = _shad_depth;
                     }
                     else
		     {
			this->_sides[ii] += _g_offs[ii];
                        _shad_zones[ii][0] = _g_offs[ii];
			_g_offs[ii] = 0;
		     }
                     if( _g_offs[ii] + this->_sides[ii] < _g_sides[ii] - _shad_depth )
                     {
                        this->_sides[ii] += _shad_depth;
                        _shad_zones[ii][1] = this->_sides[ii] - _shad_depth;
                     }
                     else
		     {
                        _shad_zones[ii][1] = this->_sides[ii];
			this->_sides[ii] += _g_sides[ii] - (_g_offs[ii] + this->_sides[ii]);
		     }
                  }
		  LOGDLN( "Domain offset: ", _g_offs );
                  LOGDLN( "Domain size: ", this->_sides );
                  LOGDLN( "Shadow zones: ", _shad_zones );

                  // Update my cell count.
                  this->_n_cells = 1;
                  for( unsigned ii = 0; ii < n_dims; ++ii )
                     this->_n_cells *= this->_sides[ii];
		  LOGDLN( "Domain cells: ", this->_n_cells );

                  // Do a three-phase operation. First, count the number of neighboring
                  // procs. Second, count the number of cells shadowed by each proc. Third,
                  // build tables of those cells.
                  std::vector<uint64_t> halo_cells, shad_cells;
		  std::vector<uint32_t> halo_cnts;
                  std::array<unsigned,n_dims> proc_shad_crd;
                  std::set<unsigned> nbrs;
                  for( unsigned pass = 0; pass < 3; ++pass )
                  {
                     if( pass == 1 )
                     {
			LOGDLN( "Neighbors: ", nbrs );
                        _vct.set_comm( hpc::mpi::comm::world );
                        _vct.set_neighbors( std::move( nbrs ) );
                        halo_cnts.resize( _vct.n_neighbors() );
                        std::fill( halo_cnts.begin(), halo_cnts.end(), 0 );
                     }
                     else if( pass == 2 )
                     {
                        hpc::counts_to_displs_resize( halo_cnts, _halo_displs );
                        std::fill( halo_cnts.begin(), halo_cnts.end(), 0 );
			if( _halo_displs.size() )
			   halo_cells.resize( _halo_displs.back() );
                     }

                     for( unsigned ii = 0; ii < this->_n_cells; ++ii )
                     {
                        auto crd = this->lift( ii );
                        for( unsigned jj = 0; jj < n_dims; ++jj )
                        {
                           if( crd[jj] < _shad_zones[jj][0] )
                              proc_shad_crd[jj] = proc_crd[jj] - 1;
                           else if( crd[jj] >= _shad_zones[jj][1] )
                              proc_shad_crd[jj] = proc_crd[jj] + 1;
                           else
                              proc_shad_crd[jj] = proc_crd[jj];
                        }
                        unsigned shad_proc = _proc_grid.project( proc_shad_crd.begin() );
			if( shad_proc == rank )
			   continue;

                        if( pass == 0 )
                           nbrs.insert( shad_proc );
                        else if( pass == 1 )
                           ++halo_cnts[_vct.rank_to_nbr( shad_proc )];
                        else if( pass == 2 )
                        {
                           unsigned nbr = _vct.rank_to_nbr( shad_proc );
                           halo_cells[_halo_displs[nbr] + halo_cnts[nbr]++] = to_global( ii );
                        }
                     }
                  }

                  // Scatter global indices to neighbors.
                  _vct.scattera( halo_cells, _halo_displs, shad_cells, _shad_displs );

                  // Convert all global indices to local.
                  _halo_cells.resize( halo_cells.size() );
                  _shad_cells.resize( shad_cells.size() );
                  for( unsigned ii = 0; ii < _halo_cells.size(); ++ii )
                     _halo_cells[ii] = to_local( halo_cells[ii] );
                  for( unsigned ii = 0; ii < _shad_cells.size(); ++ii )
                     _shad_cells[ii] = to_local( shad_cells[ii] );
		  LOGDLN( "Halo displs: ", _halo_displs );
		  LOGDLN( "Halo cells: ", _halo_cells );
		  LOGDLN( "Shadow displs: ", _shad_displs );
		  LOGDLN( "Shadow cells: ", _shad_cells );

		  // Calculate my coordinate offset.
		  for( unsigned ii = 0; ii < n_dims; ++ii )
		  {
		     _l_min[ii] = _g_min[ii] + static_cast<double>( _l_offs[ii] )*this->_delta[ii];
		     _l_max[ii] = _g_min[ii] + static_cast<double>( _l_offs[ii] + _l_sides[ii] )*this->_delta[ii];
		     this->_min[ii] = _g_min[ii] + static_cast<double>( _g_offs[ii] )*this->_delta[ii];
		     this->_max[ii] = _g_min[ii] + static_cast<double>( _g_sides[ii] )*this->_delta[ii];
		  }
		  LOGDLN( "Local minimum: ", _l_min );
		  LOGDLN( "Local maximum: ", _l_max );
		  LOGDLN( "Domain minimum: ", this->_min );
		  LOGDLN( "Domain maximum: ", this->_max );
		  LOGDLN( "Global minimum: ", _g_min );
		  LOGDLN( "Global maximum: ", _g_max );
               }
            }

         protected:

            uint64_t _n_g_cells;
	    uint32_t _n_l_cells;
            std::array<uint32_t,n_dims> _l_offs;
            std::array<uint16_t,n_dims> _l_sides;
            std::array<uint32_t,n_dims> _g_offs;
            std::array<uint32_t,n_dims> _g_sides;
            hpc::grid<unsigned> _proc_grid;
            hpc::mpi::vct _vct;
            uint16_t _shad_depth;
            std::array<std::array<uint16_t,2>,n_dims> _shad_zones;
            std::vector<uint32_t> _halo_cells;
            std::vector<uint64_t> _halo_displs;
            std::vector<uint32_t> _shad_cells;
            std::vector<uint64_t> _shad_displs;
	    std::array<double,n_dims> _g_min;
	    std::array<double,n_dims> _g_max;
	    std::array<double,n_dims> _l_min;
	    std::array<double,n_dims> _l_max;
	    std::array<double,n_dims> _crd_offs;
            hpc::mpi::comm* _comm;
         };

      }
   }
}

#endif
