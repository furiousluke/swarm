#ifndef swarm_xdmf_hh
#define swarm_xdmf_hh

#include <string>
#include <libhpc/system/stream_indent.hh>
#include <libhpc/h5/file.hh>
#include "tuple.hh"

namespace swarm {

   template< class Population >
   void
   write_xdmf( Population const& pop,
	       std::string const& base = "swarm",
	       std::string const& name = "swarm" )
   {
      static constexpr unsigned n_dims = Population::n_dims;
      auto const* grid = pop.grid();

      if( hpc::mpi::comm::world.rank() == 0 )
      {
	 std::ofstream xml_file( base + ".xmf" );
	 EXCEPT( xml_file.good(), "Unable to open XDMF XML file: ", base + ".xmf" );
	 xml_file << hpc::indent << "<Xdmf Version=\"2.0\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << "<Domain>\n" << hpc::setindent( 1 );

	 xml_file << hpc::indent << "<Grid Name=\"" << name + "_particles" << "\" GridType=\"Uniform\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << "<Topology TopologyType=\"Polyvertex\" ";
	 xml_file << "NumberOfElements=\"" << pop.global_size() << "\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << "<DataItem Format=\"HDF\" NumberType=\"UINT\" ";
	 xml_file << "Dimensions=\"" << pop.global_size() << "\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << base << ".h5:/topology\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</DataItem>\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Topology>\n";

	 static char const* geometry_map[4] = { "", "", "XY", "XYZ" };
	 xml_file << hpc::indent << "<Geometry GeometryType=\"" << geometry_map[n_dims] << "\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << "<DataItem Format=\"HDF\" NumberType=\"Float\" ";
	 xml_file << "Dimensions=\"" << pop.global_size() << " " << n_dims << "\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << base << ".h5:/geometry\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</DataItem>\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Geometry>\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Grid>\n";

	 xml_file << hpc::indent << "<Grid Name=\"" << name + "_grid" << "\" GridType=\"Uniform\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << "<Topology TopologyType=\"2DCoRectMesh\" Dimensions=\"";
	 xml_file << grid->sides()[0] + 1 << " " << grid->sides()[1] + 1;
	 if( n_dims == 3 )
	    xml_file << grid->sides()[2];
	 xml_file << "\" />\n";
	 static char const* grid_geometry_map[4] = { "", "", "ORIGIN_DXDY", "ORIGIN_DXDYDZ" };
	 xml_file << hpc::indent << "<Geometry GeometryType=\"" << grid_geometry_map[n_dims] << "\">\n" << hpc::setindent( 1 );
	 xml_file << hpc::indent << "<DataItem Format=\"XML\" NumberType=\"Float\" ";
	 xml_file << "Dimensions=\"" << n_dims << "\"> ";
	 for( unsigned ii = 0; ii < n_dims; ++ii )
	    xml_file << grid->min()[0] << " ";
	 xml_file << "</DataItem>\n";
	 xml_file << hpc::indent << "<DataItem Format=\"XML\" NumberType=\"Float\" ";
	 xml_file << "Dimensions=\"" << n_dims << "\"> ";
	 for( unsigned ii = 0; ii < n_dims; ++ii )
	    xml_file << grid->delta()[0] << " ";
	 xml_file << "</DataItem>\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Geometry>\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Grid>\n";

	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Domain>\n";
	 xml_file << hpc::setindent( -1 ) << hpc::indent << "</Xdmf>\n";
      }

      hpc::mpi::comm::world.barrier();

      hpc::h5::file h5_file( base + ".h5", H5F_ACC_TRUNC, hpc::mpi::comm::world );
      {
	 unsigned long long offs = hpc::mpi::comm::world.scan( (unsigned long long)pop.size() );
	 std::vector<unsigned long long> topo( pop.size() );
	 std::iota( topo.begin(), topo.end(), offs );
	 h5_file.write<unsigned long long>( "topology", topo );
      }
      {
	 std::vector<swarm::pos_type> geom( n_dims*pop.size() );
	 for( unsigned ii = 0; ii < pop.size(); ++ii )
	 {
	    geom[ii*n_dims + 0] = swarm::get<0>( pop[ii].data );
	    geom[ii*n_dims + 1] = swarm::get<1>( pop[ii].data );
	    if( n_dims == 3 )
	       geom[ii*n_dims + 2] = swarm::get<2>( pop[ii].data );
	 }
	 h5_file.write<swarm::pos_type>( "geometry", geom );
      }
   }

}

#endif
