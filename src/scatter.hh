#ifndef swarm_scatter_hh
#define swarm_scatter_hh

#include <vector>
#include <unordered_map>
#include <libhpc/debug/assert.hh>
#include <libhpc/debug/except.hh>
#include <libhpc/containers/view.hh>
#include <libhpc/mpi/comm.hh>
#include <libhpc/mpi/data_type.hh>
#include <libhpc/containers/helpers.hh>

namespace swarm {

   template< class Grid,
	     class DisplVec,
	     class DataVec,
	     class CellsVec >
   void
   scatter( Grid const& grid,
	    DisplVec& displs,
	    DataVec& data,
	    CellsVec& cells )
   {
      typedef typename DisplVec::value_type displ_type;
      typedef typename DataVec::value_type data_type;
      typedef typename CellsVec::value_type cell_type;

      // Send shadow cell particle counts to neighbors.
      std::vector<unsigned> shad_cnts( grid.n_shadow_cells() );
      {
	 unsigned cur = 0;
	 for( unsigned ii = 0; ii < grid.vct().n_neighbors(); ++ii )
	 {
	    auto shad_cells = grid.shadow_cells( ii );
	    for( unsigned jj = 0; jj < shad_cells.size(); ++jj )
	    {
	       auto idx = shad_cells[jj];
	       shad_cnts[cur++] = displs[idx + 1] - displs[idx];
	    }
	 }
      }
      std::vector<unsigned> halo_cnts( grid.n_halo_cells() );
      grid.vct().scatter<unsigned,uint64_t>( shad_cnts, grid.shadow_displs(), halo_cnts, grid.halo_displs() );
      LOGDLN( "Outgoing displs: ", grid.shadow_displs() );
      LOGDLN( "Outgoing sizes:  ", shad_cnts );
      LOGDLN( "Incoming displs: ", grid.halo_displs() );
      LOGDLN( "Incoming sizes:  ", halo_cnts );

      // Resize.
#ifndef NDEBUG
      for( unsigned ii = 0; ii < grid.vct().n_neighbors(); ++ii )
      {
	 uint64_t offs = grid.halo_displs()[ii];
	 uint64_t size = grid.halo_displs()[ii + 1] - offs;
	 ASSERT( hpc::is_ordered( hpc::view<std::vector<uint32_t>>::type( grid.halo_cells(), size, offs ) ),
		 "Halo cells should be ordered." );
      }
#endif
      uint64_t old_size = data.size();
      uint64_t new_size = std::accumulate( halo_cnts.begin(), halo_cnts.end(), 0 ) + old_size;
      LOGDLN( "Resizing arrays: old size = ", old_size, ", new size = ", new_size );
      EXCEPT( new_size <= data.capacity(), "Particle array is too small for halo data with size: ", new_size,
	      ", old size: ", old_size, ", capacity: ", data.capacity() );
      data.resize( new_size );
      if( !cells.empty() )
	 cells.resize( new_size );
      LOGILN( "Array occupancy factor: ", (double)new_size/(double)old_size - 1.0 );

      // Perform array shuffle.
      {
	 LOGBLOCKD( "Shuffling arrays." );

	 LOGDLN( "Building mapping." );
	 std::unordered_map<uint32_t,uint32_t> halo_cell_map;
	 for( unsigned ii = 0; ii < grid.halo_cells().size(); ++ii )
	    halo_cell_map.emplace( grid.halo_cells()[ii], ii );

#ifndef NDEBUG
	 // Sanity check. Make sure all particles are accounted for
	 // before going in.
	 uint64_t local_check = 0;
	 {
	    for( unsigned ii = 0; ii < displs.size() - 1; ++ii )
	    {
	       if( halo_cell_map.find( ii ) == halo_cell_map.end() )
	       {
		  ASSERT( grid.is_local( ii ), "Must be local cell." );
		  local_check += displs[ii + 1] - displs[ii];
	       }
	       else
	       {
		  ASSERT( !grid.is_local( ii ), "Must not be local cell." );
		  ASSERT( (displs[ii + 1] - displs[ii]) == 0, "Can't be anything in halo cells." );
	       }
	    }
	    ASSERT( local_check == old_size );
	 }
	 local_check = 0;
#endif

	 uint64_t from = old_size, to = new_size;
	 uint32_t cell = displs.size() - 1;
	 while( cell > 0 )
	 {
	    LOGBLOCKD( "Processing cell: ", cell - 1 );
	    LOGDLN( "From: ", from );
	    LOGDLN( "To:   ", to );
	    if( halo_cell_map.find( cell - 1 ) == halo_cell_map.end() )
	    {
	       LOGDLN( "Is a local cell." );
	       uint64_t cur_cell_size = displs[cell] - displs[cell - 1];
	       LOGDLN( "Cell size: ", cur_cell_size );
	       while( cur_cell_size > 0 )
	       {
		  ASSERT( to > 0 );
		  ASSERT( from > 0 );
		  ASSERT( to - 1 < data.size() );
		  ASSERT( from - 1 < data.size() );
		  data[to - 1] = data[from - 1];
		  if( !cells.empty() )
		  {
		     ASSERT( to - 1 < cells.size() );
		     ASSERT( from - 1 < cells.size() );
		     cells[to - 1] = cells[from - 1];
		  }
		  --from;
		  --to;
		  --cur_cell_size;
#ifndef NDEBUG
		  ++local_check;
#endif
	       }
	    }
	    else
	    {
	       LOGDLN( "Is a halo cell." );
	       LOGDLN( "Cell size: ", halo_cnts[halo_cell_map.at( cell - 1 )] );
	       if( !cells.empty() )
	       {
		  for( unsigned ii = 0; ii < halo_cnts[halo_cell_map.at( cell - 1 )]; ++ii, --to )
		  {
		     ASSERT( to > 0 );
		     ASSERT( to - 1 < cells.size() );
		     cells[to - 1] = cell - 1;
		  }
	       }
	       else
	       {
		  ASSERT( to >= halo_cnts[halo_cell_map.at( cell - 1 )] );
		  to -= halo_cnts[halo_cell_map.at( cell - 1 )];
	       }
	    }
	    --cell;
	 }
	 ASSERT( from == 0 );
	 ASSERT( to == 0 );
	 ASSERT( local_check == old_size );
      }

      // TODO: REMOVE
      hpc::mpi::comm::world.barrier();

      // Update displacements.
      {
	 LOGBLOCKD( "Updating displacements." );
	 hpc::displs_to_counts<decltype(displs)>( displs );
	 for( unsigned ii = 0; ii < halo_cnts.size(); ++ii )
	    displs[grid.halo_cells()[ii]] = halo_cnts[ii];
	 hpc::counts_to_displs<decltype(displs)>( displs );
      }

      // Create appropriate data types to transfer into correct memory.
      {
	 LOGBLOCKD( "Transferring." );
	 std::vector<hpc::mpi::data_type> shad_types( grid.vct().n_neighbors() );
	 std::vector<hpc::mpi::data_type> halo_types( grid.vct().n_neighbors() );
	 for( unsigned ii = 0; ii < grid.vct().n_neighbors(); ++ii )
	 {
	    shad_types[ii].indexed_csr( displs, grid.shadow_cells( ii ), hpc::mpi::data_type::byte, sizeof(data_type) );
	    halo_types[ii].indexed_csr( displs, grid.halo_cells( ii ), hpc::mpi::data_type::byte, sizeof(data_type) );
	 }

	 // Transfer shadows.
	 grid.vct().scatter_types( data.data(), shad_types.begin(), data.data(), halo_types.begin() );
      }
   }

}

#endif
