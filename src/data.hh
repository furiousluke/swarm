#ifndef swarm_data_hh
#define swarm_data_hh

#include <iostream>
#include <tuple>
#include <type_traits>
#include "types.hh"

namespace swarm {
   namespace data {

      template< unsigned D,
                class... Args >
      struct base;

      template< class... Args >
      struct base<2,Args...>
      {
         static constexpr unsigned n_dims = 2;
         struct constant_h : std::false_type {};
         struct massless   : std::false_type {};
	 std::tuple<real_type,real_type,real_type,Args...> data;
      };

      template< class... Args >
      struct base<3,Args...>
      {
         static constexpr unsigned n_dims = 3;
         struct constant_h : std::false_type {};
         struct massless   : std::false_type {};
	 std::tuple<real_type,real_type,real_type,real_type,Args...> data;
      };

      template< unsigned D,
                class... Args >
      struct fixed
         : public base<D,Args...>
      {
         struct constant_h : std::true_type {};
         struct massless   : std::true_type {};
      };

      template< unsigned D,
                class... Args >
      struct adaptive
         : public base<D,real_type,Args...>
      {
         struct constant_h : std::false_type {};
         struct massless   : std::true_type  {};
      };

      template< unsigned D,
                class... Args >
      struct massive
         : public base<D,real_type,Args...>
      {
         struct constant_h : std::true_type  {};
         struct massless   : std::false_type {};
      };

      template< unsigned D,
                class... Args >
      struct full
         : public base<D,real_type,real_type,Args...>
      {
         struct constant_h : std::false_type {};
         struct massless   : std::false_type {};
      };

   }
}

namespace std {

   // template< unsigned I,
   //           class... Args >
   // auto
   // get( swarm::data::base<Args...>& obj ) -> decltype( std::get<I>( obj.data ) )
   // {
   //    return std::get<I>( obj.data );
   // }

   // template< unsigned I,
   //           class... Args >
   // auto
   // get( swarm::data::base<Args...> const& obj ) -> decltype( std::get<I>( obj.data ) )
   // {
   //    return std::get<I>( obj.data );
   // }

}

template< unsigned D,
	  class... Args >
std::ostream&
operator<<( std::ostream& strm,
            swarm::data::base<D,Args...> const& part )
{
   strm << part.data;
   return strm;
}

#endif
