#ifndef swarm_swarm_hh
#define swarm_swarm_hh

#include <swarm/types.hh>
#include <swarm/data.hh>
#include <swarm/grid.hh>
#include <swarm/population.hh>
#include <swarm/populate.hh>
#include <swarm/summation.hh>
#include <swarm/interaction.hh>
#include <swarm/kernel.hh>
#include <swarm/utils.hh>
#include <swarm/mpi/grid.hh>
#include <swarm/xdmf.hh>

#endif
