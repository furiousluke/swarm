#ifndef swarm_locate_hh
#define swarm_locate_hh

#include <tuple>
#include <vector>
#include <limits>
#include <stdint.h>
#include <libhpc/algorithm/morton.hh>
#include <libhpc/debug/assert.hh>
#include "data.hh"
#include "tuple.hh"
#include "types.hh"

namespace swarm {

   template< int D >
   struct locate_impl;

   template<>
   struct locate_impl<2>
   {
      template< class DataIter,
                class Grid,
                class BinsSeq >
      static
      void
      locate( DataIter data_begin,
              DataIter const& data_end,
              Grid const& grid,
              BinsSeq& bins )
      {
         std::array<uint16_t,2> crd;
         std::array<pos_type,2> h_inv{ 1.0/grid.delta()[0], 1.0/grid.delta()[1] };
         for( unsigned ii = 0; data_begin != data_end; ++data_begin, ++ii )
         {
            auto const& pnt = *data_begin;
            crd[0] = (swarm::get<0>( pnt.data ) - grid.min()[0])*h_inv[0];
            crd[1] = (swarm::get<1>( pnt.data ) - grid.min()[1])*h_inv[1];
	    if( crd[0] == grid.sides()[0] )
	       --crd[0];
	    if( crd[1] == grid.sides()[1] )
	       --crd[1];
            bins[ii] = grid.project( crd );
         }
      }
   };

   template<>
   struct locate_impl<3>
   {
      template< class DataIter,
                class Grid,
                class BinsSeq >
      static
      void
      locate( DataIter data_begin,
              DataIter const& data_end,
              Grid const& grid,
              BinsSeq& bins )
      {
         std::array<uint16_t,3> crd;
         std::array<pos_type,3> h_inv{ 1.0/grid.delta()[0], 1.0/grid.delta()[1], 1.0/grid.delta()[2] };
         for( unsigned ii = 0; data_begin != data_end; ++data_begin, ++ii )
         {
            auto const& pnt = *data_begin;
            crd[0] = (swarm::get<0>( pnt.data ) - grid.min()[0])*h_inv[0];
	    crd[1] = (swarm::get<1>( pnt.data ) - grid.min()[1])*h_inv[1];
	    crd[2] = (swarm::get<2>( pnt.data ) - grid.min()[2])*h_inv[2];
	    if( crd[0] == grid.sides()[0] )
	       --crd[0];
	    if( crd[1] == grid.sides()[1] )
	       --crd[1];
	    if( crd[2] == grid.sides()[2] )
	       --crd[2];
            bins[ii] = grid.project( crd );
         }
      }
   };

   template< int D,
             class DataIter,
             class Grid,
             class BinsSeq >
   void
   locate( DataIter data_begin,
           DataIter const& data_end,
           Grid const& grid,
           BinsSeq& bins )
   {
      locate_impl<D>::locate( data_begin, data_end, grid, bins );
   }

   template< class BinSeq,
             class DisplSeq >
   void
   calc_cell_displs( BinSeq const& bins,
                     DisplSeq& displs )
   {
      typedef typename DisplSeq::value_type displ_type;

      std::fill( displs.begin(), displs.end(), std::numeric_limits<displ_type>::max() );
      displs[bins[0]] = 0;
      for( unsigned ii = 1; ii < bins.size(); ++ii )
      {
         if( bins[ii - 1] != bins[ii] )
	 {
	    ASSERT( bins[ii] < displs.size(), "Invalid write to displacements array at index: ", bins[ii] );
            displs[bins[ii]] = ii;
	 }
      }

      {
         unsigned ii = displs.size() - 1;
         displs[ii] = bins.size();
         for( ; ii > 0; --ii )
         {
            if( displs[ii - 1] == std::numeric_limits<displ_type>::max() )
               displs[ii - 1] = displs[ii];
         }
      }
   }

}

#endif
