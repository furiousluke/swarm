#ifndef swarm_tuple_hh
#define swarm_tuple_hh

#include <tuple>
#if HAVE_THRUST
#include <thrust/tuple.h>
#endif

namespace swarm {

   template< unsigned I,
	     class... Args >
   auto
   get( std::tuple<Args...>& obj ) -> decltype( std::get<I>( obj ) )
   {
      return std::get<I>( obj );
   }

   template< unsigned I,
	     class... Args >
   auto
   get( std::tuple<Args...> const& obj ) -> decltype( std::get<I>( obj ) )
   {
      return std::get<I>( obj );
   }

#if HAVE_THRUST

   template< unsigned I,
	     class... Args >
   auto
   get( thrust::tuple<Args...>& obj ) -> decltype( thrust::get<I>( obj ) )
   {
      return thrust::get<I>( obj );
   }

   template< unsigned I,
	     class... Args >
   auto
   get( thrust::tuple<Args...> const& obj ) -> decltype( thrust::get<I>( obj ) )
   {
      return thrust::get<I>( obj );
   }

#endif

}

#endif
