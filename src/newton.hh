#ifndef swarm_newton_hh
#define swarm_newton_hh

#include "kernel.hh"
#include "types.hh"

namespace swarm {

   template< class Summation,
	     class Interaction,
             class Kernel >
   struct newton_function
   {
      typedef          Summation                  summation_type;
      typedef typename Summation::population_type population_type;
      typedef typename population_type::data_type data_type;
      typedef          Interaction                interaction_type;
      typedef          Kernel                     kernel_type;
      static constexpr unsigned                   n_dims = data_type::n_dims;

      newton_function( summation_type& sum,
		       interaction_type const& inter,
                       kernel_type const& kern,
                       uint32_t cell = 0,
                       data_type* data_i = nullptr,
                       h_type eta = 1.0 )
         : sum( sum ),
	   inter( inter ),
           kern( kern ),
           cell( cell ),
           data_i( data_i ),
           eta( eta )
      {
      }

      void
      set_cell( uint32_t cell )
      {
         this->cell = cell;
      }

      void
      set_data( data_type& data_i )
      {
         this->data_i = &data_i;
      }

      h_type
      operator()( h_type h )
      {
         // Run the density summation.
         pos_type r_cut = h*Kernel::radius;
         sum.sum_particle( inter, kern, cell, h, r_cut, *data_i );

         // Calculate f_n, dfdh and dh.
         density_type rho = std::get<n_dims>( data_i->data );
	 LOGTLN( "Density: ", rho );
         new_h = eta*pow( 1.0/rho, 1.0/(density_type)n_dims );
	 LOGTLN( "New h: ", new_h );
         h_type f_n = h - new_h;

	 return f_n;
      }

      h_type
      derivative( h_type h,
                  h_type f ) const
      {
         density_type rho = std::get<n_dims>( data_i->data );
         return (eta*sum.deriv_h()*pow( 1.0/rho, 1/(density_type)n_dims - 1.0 ))/(((density_type)n_dims)*rho*rho) + 1.0;
      }

      h_type
      r_cut( h_type h ) const
      {
         return h*Kernel::radius;
      }

      summation_type& sum;
      interaction_type const& inter;
      kernel_type const& kern;
      uint32_t cell;
      data_type* data_i;
      h_type eta;
      h_type new_h;
   };

}

#endif
