#ifndef swarm_population_hh
#define swarm_population_hh

#include <iostream>
#include <vector>
#include <algorithm>
#include <type_traits>
#include <memory>
#include <stdint.h>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/type_traits/is_convertible.hpp>
#include <boost/utility/enable_if.hpp>
#include <libhpc/containers/sort_permute_iter.hh>
#include <libhpc/containers/deallocate.hh>
#include <libhpc/containers/counts.hh>
#include <libhpc/mpi/comm.hh>
#include "locate.hh"
#include "scatter.hh"
#include "tuple.hh"
#include "types.hh"

#ifdef USE_CUDA_SORT
void
cuda_sort( thrust::host_vector<data_type>& data,
	   thrust::host_vector<uint32_t>& cells );

#endif

namespace swarm {

   template< class Data,
	     class Value >
   class population_cell_iterator
      : public boost::iterator_facade< population_cell_iterator<Data,Value>,
				       Value,
				       std::forward_iterator_tag >
   {
      friend class boost::iterator_core_access;
      template<class,class> friend class population_cell_iterator;

      struct enabler {};

   public:

      typedef Value       value_type;
      typedef value_type& reference_type;

   public:

      population_cell_iterator( unsigned idx,
				Data& data )
	 : _idx( idx ),
	   _data( &data )
      {
      }

      template< class OtherData,
		class OtherValue >
      population_cell_iterator( population_cell_iterator<OtherData,OtherValue> const& other,
				typename boost::enable_if<
				boost::is_convertible<OtherData*,Data*>,enabler
				>::type = enabler() )
	 : _idx( other._idx ),
	   _data( other._data )
      {
      }

      unsigned
      index() const
      {
	 return _idx;
      }

   protected:

      void
      increment()
      {
	 ++_idx;
      }

      reference_type
      dereference() const
      {
	 return (*_data)[_idx];
      }

      template< class OtherData,
		class OtherValue >
      bool
      equal( population_cell_iterator<OtherData,OtherValue> const& other ) const
      {
	 return _idx == other._idx;
      }

   protected:

      unsigned _idx;
      Data* _data;
   };

   class empty {};

   class constant_h
   {
   public:

      void
      set_smoothing_length( h_type h )
      {
	 _h = h;
      }

      h_type
      smoothing_length() const
      {
	 return _h;
      }

   protected:

      h_type _h;
   };

   template< class Grid,
	     class Data,
	     template<class,class> class Vector = std::vector >
   class population
      : public std::conditional<Data::constant_h::value,constant_h,empty>::type
   {
   public:

      typedef          Grid     grid_type;
      typedef          Data     data_type;
      static constexpr unsigned n_dims     = data_type::n_dims;

      typedef          Vector<data_type,std::allocator<data_type>> vector_type;
      typedef typename vector_type::iterator iterator;

   public:

      population( grid_type const& grid,
		  float fac = 0.0 )
	 : _grid( &grid ),
	   _fac( fac )
      {
      }

      grid_type const*
      grid() const
      {
	 return _grid;
      }

      void
      resize( unsigned size )
      {
	 hpc::deallocate( _data );
	 unsigned full_size = size*(1.0 + _fac);
	 _data.reserve( full_size );
	 _data.resize( size );
	 _g_size = size;
	 _g_size = hpc::mpi::comm::world.all_reduce( _g_size );
      }

      unsigned
      size() const
      {
	 return _data.size();
      }

      unsigned long long
      global_size() const
      {
	 return _g_size;
      }

      vector_type&
      data()
      {
	 return _data;
      }

      iterator
      begin()
      {
	 return _data.begin();
      }

      iterator
      end()
      {
	 return _data.end();
      }

      data_type&
      operator[]( unsigned idx )
      {
	 return _data[idx];
      }

      data_type const&
      operator[]( unsigned idx ) const
      {
	 return _data[idx];
      }

      void
      update()
      {
	 _avg_h = 0.0;
	 _min[0] = std::numeric_limits<swarm::real_type>::max();
	 _min[1] = std::numeric_limits<swarm::real_type>::max();
	 _min[2] = std::numeric_limits<swarm::real_type>::max();
	 _max[0] = std::numeric_limits<swarm::real_type>::min();
	 _max[1] = std::numeric_limits<swarm::real_type>::min();
	 _max[2] = std::numeric_limits<swarm::real_type>::min();
	 for( auto const& part : _data )
	 {
	    _avg_h += swarm::get<n_dims + 1>( part.data );
	    _min[0] = std::min<swarm::real_type>( swarm::get<0>( part.data ), _min[0] );
	    _min[1] = std::min<swarm::real_type>( swarm::get<1>( part.data ), _min[1] );
	    _min[2] = std::min<swarm::real_type>( swarm::get<2>( part.data ), _min[2] );
	    _max[0] = std::max<swarm::real_type>( swarm::get<0>( part.data ), _max[0] );
	    _max[1] = std::max<swarm::real_type>( swarm::get<1>( part.data ), _max[1] );
	    _max[2] = std::max<swarm::real_type>( swarm::get<2>( part.data ), _max[2] );
	 }
	 for( unsigned ii = 0; ii < n_dims; ++ii )
	 {
	    _min[ii] -= 1e-8;
	    _max[ii] += 1e-8;
	 }
	 _avg_h /= (swarm::real_type)_data.size();
      }

      std::array<swarm::real_type,n_dims> const&
      min() const
      {
	 return _min;
      }

      std::array<swarm::real_type,n_dims> const&
      max() const
      {
	 return _max;
      }

      swarm::real_type
      mean_h() const
      {
	 return _avg_h;
      }

      friend
      std::ostream&
      operator<<( std::ostream& strm,
		  population const& obj )
      {
	 strm << obj._data;
	 return strm;
      }

   protected:

      grid_type const* _grid;
      vector_type _data;
      std::array<swarm::real_type,n_dims> _min;
      std::array<swarm::real_type,n_dims> _max;
      swarm::real_type _avg_h;
      unsigned long long _g_size;
      float _fac;
   };

   namespace populations {

      template< class Grid,
		class Data,
		template<class,class> class Vector = std::vector >
      class ordered
	 : public population<Grid,Data,Vector>
      {
      public:

	 typedef population<Grid,Data,Vector> super_type;

	 typedef          Grid         grid_type;
	 typedef          Data         data_type;
	 typedef          Vector<Data,std::allocator<Data>> vector_type;
	 typedef population_cell_iterator<vector_type,       data_type>       cell_iterator;
	 typedef population_cell_iterator<vector_type const, data_type const> cell_const_iterator;

	 static constexpr unsigned n_dims = data_type::n_dims;

      public:

	 ordered( grid_type const& grid,
		  double fac = 0.0,
		  bool keep_cells = false )
	    : super_type( grid, fac ),
	      _keep_cells( keep_cells )
	 {
	 }

	 void
	 locate()
	 {
	    _cells.resize( this->_data.size() );
	    swarm::locate<n_dims>( this->_data.begin(), this->_data.end(), *this->_grid, _cells );
#ifdef USE_CUDA_SORT
	    cuda_sort( this->_data, _cells );
#else
	    std::sort(
	       hpc::make_sort_permute_iter( _cells.begin(), this->_data.begin() ),
	       hpc::make_sort_permute_iter( _cells.end(),   this->_data.end()   ),
	       hpc::sort_permute_iter_compare<std::vector<uint32_t>::iterator,typename vector_type::iterator>()
	       );
#endif
	    _displs.resize( this->_grid->n_cells() + 1 ),
	    swarm::calc_cell_displs( _cells, _displs );
	    if( !_keep_cells )
	       hpc::deallocate( _cells );

// #ifndef NDEBUG
// 	    unsigned cnt = 0;
// 	    for( unsigned ii = 0; ii < _displs.size() - 1; ++ii )
// 	    {
// 	       for( unsigned jj = _displs[ii]; jj < _displs[ii + 1]; ++jj )
// 	       {
// 		  ++cnt;
// 		  ASSERT( swarm::get<0>( this->_data[jj].data ) - 1e-8 >=
// 			  this->_grid->min()[0] + this->_grid->lift( ii )[0]*this->_grid->delta()[0],
// 			  "x coordinate, ", swarm::get<0>( this->_data[jj].data ), ", outside cell "
// 			  "minimum of ", this->_grid->min()[0] + this->_grid->lift( ii )[0]*this->_grid->delta()[0] );
// 		  ASSERT( swarm::get<1>( this->_data[jj].data ) - 1e-8 >=
// 			  this->_grid->min()[1] + this->_grid->lift( ii )[1]*this->_grid->delta()[1],
// 			  "y coordinate, ", swarm::get<1>( this->_data[jj].data ), ", outside cell "
// 			  "minimum of ", this->_grid->min()[1] + this->_grid->lift( ii )[1]*this->_grid->delta()[1] );
// 		  if( n_dims == 3 )
// 		  {
// 		     ASSERT( swarm::get<1>( this->_data[jj].data ) - 1e-8 >=
// 			     this->_grid->min()[1] + this->_grid->lift( ii )[1]*this->_grid->delta()[1] );
// 		  }
// 		  ASSERT( swarm::get<0>( this->_data[jj].data ) + 1e-8 <=
// 			  this->_grid->min()[0] + (this->_grid->lift( ii )[0] + 1)*this->_grid->delta()[0],
// 			  "x coordinate, ", swarm::get<0>( this->_data[jj].data ), ", outside cell "
// 			  "maximum of ", this->_grid->min()[0] + (this->_grid->lift( ii )[0] + 1)*this->_grid->delta()[0] );
// 		  ASSERT( swarm::get<1>( this->_data[jj].data ) + 1e-8 <=
// 			  this->_grid->min()[1] + (this->_grid->lift( ii )[1] + 1)*this->_grid->delta()[1],
// 			  "y coordinate, ", swarm::get<1>( this->_data[jj].data ), ", outside cell "
// 			  "maximum of ", this->_grid->min()[1] + (this->_grid->lift( ii )[1] + 1)*this->_grid->delta()[1] );
// 		  if( n_dims == 3 )
// 		  {
// 		     ASSERT( swarm::get<1>( this->_data[jj].data ) + 1e-8 <=
// 			     this->_grid->min()[1] + (this->_grid->lift( ii )[1] + 1)*this->_grid->delta()[1] );
// 		  }
// 	       }
// 	    }
// #endif
	 }

	 template< class SubGrid >
	 typename std::enable_if<SubGrid::is_parallel == true>::type
	 scatter()
	 {
	   swarm::scatter<SubGrid>( *this->_grid, _displs, this->_data, _cells );
	 }

	 unsigned
	 cell_size( unsigned idx ) const
	 {
	    return _displs[idx + 1] - _displs[idx];
	 }

	 data_type const&
	 cell_particle( unsigned cell,
			unsigned idx ) const
	 {
	    return this->_data[_displs[cell] + idx];
	 }

	 data_type&
	 cell_particle( unsigned cell,
			unsigned idx )
	 {
	    return this->_data[_displs[cell] + idx];
	 }

	 cell_iterator
	 begin_cell( uint32_t idx )
	 {
	    return cell_iterator( _displs[idx], this->_data );
	 }

	 cell_iterator
	 end_cell( uint32_t idx )
	 {
	    return cell_iterator( _displs[idx + 1], this->_data );
	 }

	 cell_const_iterator
	 begin_cell( uint32_t idx ) const
	 {
	    return cell_const_iterator( _displs[idx], this->_data );
	 }

	 cell_const_iterator
	 end_cell( uint32_t idx ) const
	 {
	    return cell_const_iterator( _displs[idx + 1], this->_data );
	 }

	 Vector<uint32_t,std::allocator<uint32_t>> const&
	    displacements() const
	 {
	    return _displs;
	 }

	 Vector<uint32_t,std::allocator<uint32_t>> const&
	    cells() const
	 {
	    return _cells;
	 }

      protected:

	 Vector<uint32_t,std::allocator<uint32_t>> _displs;
	 Vector<uint32_t,std::allocator<uint32_t>> _cells;
	 bool _keep_cells;
      };

   }
}

#endif
