#ifndef swarm_neighbors_hh
#define swarm_neighbors_hh

#include <stdint.h>
#include <algorithm>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/array.hpp>
#include <libhpc/algorithm/morton.hh>
#include "types.hh"

namespace swarm {

   template< int D,
             class Grid >
   class neighbor_cell_iterator;

   template< class Grid >
   class neighbor_cell_iterator<2,Grid>
      : public boost::iterator_facade< neighbor_cell_iterator<2,Grid>,
                                       uint32_t,
                                       std::forward_iterator_tag,
                                       uint32_t >
   {
      friend class boost::iterator_core_access;

   public:

      typedef Grid     grid_type;
      typedef uint32_t value_type;
      typedef uint32_t reference_type;

   public:

      neighbor_cell_iterator()
         : _grid( 0 )
      {
      }

      neighbor_cell_iterator( grid_type const* grid,
                              real_type r_cut,
                              uint32_t idx )
         : _grid( grid )
      {
         boost::array<uint16_t,2> org = grid->lift( idx );
#ifdef CXX_0X
         boost::array<uint16_t,2> dist{ r_cut/grid->delta()[0] + 1, r_cut/grid->delta()[1] + 1 };
#else
         boost::array<uint16_t,2> dist;
         dist[0] = r_cut/grid->delta()[0] + 1;
         dist[1] = r_cut/grid->delta()[1] + 1;
#endif
	 _low[0] = (org[0] > dist[0]) ? (org[0] - dist[0]) : 0;
         _low[1] = (org[1] > dist[1]) ? (org[1] - dist[1]) : 0;
         _upp[0] = (grid->sides()[0] - org[0] > dist[0]) ? (org[0] + dist[0] + 1) : grid->sides()[0];
         _upp[1] = (grid->sides()[1] - org[1] > dist[1]) ? (org[1] + dist[1] + 1) : grid->sides()[1];
         _crd = _low;
         _idx = _grid->project( _crd );
      }

   protected:

      void
      increment()
      {
         if( ++_crd[0] == _upp[0] )
         {
            _crd[0] = _low[0];
            if( ++_crd[1] == _upp[1] )
               return;
         }
         _idx = _grid->project( _crd );
      }

      reference_type
      dereference() const
      {
         return _idx;
      }

      bool
      equal( neighbor_cell_iterator const& other ) const
      {
         return _crd[1] == _upp[1];
      }

   protected:

      grid_type const* _grid;
      boost::array<uint16_t,2> _low, _upp;
      boost::array<uint16_t,2> _crd;
      uint32_t _idx;
   };

   template< class Grid >
   class neighbor_cell_iterator<3,Grid>
      : public boost::iterator_facade< neighbor_cell_iterator<3,Grid>,
                                       uint32_t,
                                       std::forward_iterator_tag,
                                       uint32_t >
   {
      friend class boost::iterator_core_access;

   public:

      typedef Grid     grid_type;
      typedef uint32_t value_type;
      typedef uint32_t reference_type;

   public:

      neighbor_cell_iterator()
         : _grid( 0 )
      {
      }

      neighbor_cell_iterator( grid_type const* grid,
                              real_type r_cut,
                              uint32_t idx )
         : _grid( grid )
      {
         boost::array<uint16_t,3> org = grid->lift( idx );
#ifdef CXX_0X
         boost::array<uint16_t,3> dist{ r_cut/grid->delta()[0] + 1, r_cut/grid->delta()[1] + 1, r_cut/grid->delta()[2] + 1 };
#else
         boost::array<uint16_t,3> dist;
         dist[0] = r_cut/grid->delta()[0] + 1;
         dist[1] = r_cut/grid->delta()[1] + 1;
         dist[2] = r_cut/grid->delta()[2] + 1;
#endif
	 _low[0] = (org[0] > dist[0]) ? (org[0] - dist[0]) : 0;
         _low[1] = (org[1] > dist[1]) ? (org[1] - dist[1]) : 0;
         _low[2] = (org[2] > dist[2]) ? (org[2] - dist[2]) : 0;
         _upp[0] = (grid->sides()[0] - org[0] > dist[0]) ? (org[0] + dist[0] + 1) : grid->sides()[0];
         _upp[1] = (grid->sides()[1] - org[1] > dist[1]) ? (org[1] + dist[1] + 1) : grid->sides()[1];
         _upp[2] = (grid->sides()[2] - org[2] > dist[2]) ? (org[2] + dist[2] + 1) : grid->sides()[2];
         _crd = _low;
         _idx = _grid->project( _crd );
      }

   protected:

      void
      increment()
      {
         if( ++_crd[0] == _upp[0] )
         {
            _crd[0] = _low[0];
            if( ++_crd[1] == _upp[1] )
            {
               _crd[1] = _low[1];
               if( ++_crd[2] == _upp[2] )
                  return;
            }
         }
         _idx = _grid->project( _crd );
      }

      reference_type
      dereference() const
      {
         return _idx;
      }

      bool
      equal( neighbor_cell_iterator const& other ) const
      {
         return _crd[2] == _upp[2];
      }

   protected:

      grid_type const* _grid;
      boost::array<uint16_t,3> _low, _upp;
      boost::array<uint16_t,3> _crd;
      uint32_t _idx;
   };

}

#endif
