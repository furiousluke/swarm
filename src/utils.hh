#ifndef swarm_utils_hh
#define swarm_utils_hh

#include "tuple.hh"

namespace swarm {

   template< class Population >
   void
   dump_cells( Population const& pop )
   {
      for( unsigned ii = 0; ii < pop.grid()->n_cells(); ++ii )
      {
	 for( auto it = pop.begin_cell( ii ); it != pop.end_cell( ii ); ++it )
	 {
	    auto const& part = *it;
	    std::cout << swarm::get<0>( part.data ) << " ";
	    std::cout << swarm::get<1>( part.data ) << " ";
	    if( Population::n_dims > 2 )
	       std::cout << swarm::get<2>( part.data ) << " ";
	    std::cout << ii << "\n";
	 }
      }
   }

   template< class Population >
   void
   dump_density( Population const& pop )
   {
      for( unsigned ii = 0; ii < pop.size(); ++ii )
      {
         auto const& part = pop[ii];
         std::cout << swarm::get<0>( part.data ) << ", ";
         std::cout << swarm::get<1>( part.data ) << ", ";
         if( Population::n_dims > 2 )
            std::cout << swarm::get<2>( part.data ) << ", ";
         std::cout << swarm::get<Population::n_dims>( part.data ) << "\n";
      }
   }

   template< class Population >
   void
   dump_density_and_h( Population const& pop )
   {
      for( unsigned ii = 0; ii < pop.size(); ++ii )
      {
         auto const& part = pop[ii];
         std::cout << swarm::get<0>( part.data ) << ", ";
         std::cout << swarm::get<1>( part.data ) << ", ";
         if( Population::n_dims > 2 )
            std::cout << swarm::get<2>( part.data ) << ", ";
         std::cout << swarm::get<Population::n_dims>( part.data ) << ", ";
         std::cout << swarm::get<Population::n_dims + 1>( part.data ) << "\n";
      }
   }

   template< class Population,
	     class VelVec >
   void
   dump_x_v_h_rho( Population const& pop,
		   VelVec const& v )
   {
      for( unsigned ii = 0; ii < pop.size(); ++ii )
      {
         auto const& part = pop[ii];
         std::cout << swarm::get<0>( part.data ) << ", ";
         std::cout << swarm::get<1>( part.data ) << ", ";
         if( Population::n_dims > 2 )
            std::cout << swarm::get<2>( part.data ) << ", ";
         std::cout << swarm::get<Population::n_dims>( part.data ) << ", ";
         std::cout << swarm::get<Population::n_dims + 1>( part.data ) << ", ";
	 std::cout << v[3*ii + 0] << ", " << v[3*ii + 1] << ", " << v[3*ii + 2] << "\n";
      }
   }

}

#endif
