#ifndef swarm_equation_of_state_hh
#define swarm_equation_of_state_hh

#include <libhpc/system/cuda.hh>
#include "math.hh"

namespace swarm {
   namespace eos {

      template< class T >
      class ideal
      {
      public:

	 typedef T real_type;

      public:

	 ideal( real_type k = 1.0,
		real_type rho0 = 1.0 )
	    : _k( k ),
	      _rho0( rho0 )
	 {
	 }

         CUDA_DEVICE_HOST
	 ideal( ideal const& src )
	    : _k( src._k ),
	      _rho0( src._rho0 )
	 {
	 }

         CUDA_DEVICE_HOST
	 real_type
	 operator()( real_type rho ) const
	 {
	    return _k*(rho - _rho0);
	 }

      protected:

	 real_type _k;
	 real_type _rho0;
      };

      template< class T >
      class tait
      {
      public:

	 typedef T real_type;

      public:

	 tait( real_type B,
	       real_type rho0,
	       real_type gamma = 7.0 )
	    : _B( B ),
	      _rho0( rho0 ),
	      _gamma( gamma )
	 {
	 }

         CUDA_DEVICE_HOST
	 tait( tait const& src )
	    : _B( src._B ),
	      _rho0( src._rho0 ),
	      _gamma( src._gamma )
	 {
	 }

         CUDA_DEVICE_HOST
	 real_type
	 operator()( real_type rho ) const
	 {
	    return _B*(swarm::pow( rho/_rho0, _gamma ) - 1.0);
	 }

      protected:

	 real_type _B;
	 real_type _rho0;
	 real_type _gamma;
      };

   }
}

#endif
