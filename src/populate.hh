#ifndef swarm_populate_hh
#define swarm_populate_hh

#include <cstdint>
#include "tuple.hh"
#include "types.hh"

namespace swarm {
   namespace populate {

      template< class Population >
      void
      uniform( Population& pop,
	       uint64_t size,
	       swarm::h_type h = 1.0 )
      {
	 static constexpr unsigned        n_dims = Population::n_dims;
	 static constexpr swarm::pos_type epsilon = 1e-5;
	 auto const* grid = pop.grid();

	 // uint64_t local_pop_size = size;
	 // for( unsigned ii = 0; ii < n_dims; ++ii )
	 //    local_pop_size = static_cast<double>( local_pop_size )*static_cast<double>( grid->local_sides()[ii] )/static_cast<double>( grid->global_sides()[ii] );
	 // pop.resize( (unsigned)local_pop_size );
	 pop.resize( (unsigned)size );

	 hpc::engine.seed( hpc::mpi::comm::world.rank() );
	 for( unsigned ii = 0; ii < pop.size(); ++ii )
	 {
	    auto& part = pop[ii];
	    swarm::get<0>( part.data ) = hpc::generate_uniform<swarm::pos_type>(
	       grid->min()[0] + epsilon, grid->max()[0] - epsilon
	       );
	    swarm::get<1>( part.data ) = hpc::generate_uniform<swarm::pos_type>(
	       grid->min()[1] + epsilon, grid->max()[1] - epsilon
	       );
	    if( Population::n_dims == 3 )
	    {
	       swarm::get<2>( part.data ) = hpc::generate_uniform<swarm::pos_type>(
		  grid->min()[2] + epsilon, grid->max()[2] - epsilon
		  );
	    }
	    swarm::get<n_dims>( part.data ) = 0.0;
	    swarm::get<n_dims + 1>( part.data ) = h;
	 }
      }

      template< class Population >
      void
      grid_cell( Population& pop,
		 swarm::h_type h = 1.0 )
      {
	 static constexpr unsigned n_dims = Population::n_dims;
	 auto const* grid = pop.grid();

	 pop.resize( grid->n_local_cells() );

	 hpc::engine.seed( hpc::mpi::comm::world.rank() );
	 std::array<uint16_t,n_dims> crd;
	 uint32_t part = 0;
	 for( unsigned ii = 0; ii < grid->n_cells(); ++ii )
	 {
	    if( !grid->is_local( ii ) )
	       continue;
	    crd = grid->lift( ii );
	    swarm::get<0>( pop[part].data ) = grid->min()[0] + ((swarm::pos_type)crd[0] + 0.5)*grid->delta()[0];
	    swarm::get<1>( pop[part].data ) = grid->min()[1] + ((swarm::pos_type)crd[1] + 0.5)*grid->delta()[1];
	    swarm::get<n_dims>( pop[part].data ) = 0.0;
	    swarm::get<n_dims + 1>( pop[part].data ) = h;
	    ++part;
	 }
      }

   }
}

#endif
